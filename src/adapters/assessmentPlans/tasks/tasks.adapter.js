import { TASK_TYPE_CONFIRMATION, TASK_TYPE_EXPERT, TASK_TYPE_TEST } from '@/constants/testTypes';
import { makeCommonItem } from '@/adapters/assessmentPlans/commonItem.adapter';
import { makeFileItem, makeFileItems } from '@/adapters/files/files.adapter';
import { VALUE_FOR_LIMITLESS_DEFINITION } from '@/constants/projectSettings';
import { taskIndexMock, taskSingleMock } from '@/mocks/assessmentPlans/tasks.mock';
import { roundStringNumberWithoutTrailingZeroes } from '@/services/Mathe.js';


export const taskSingleAdapter = (item, isMock) => {
  if (isMock) item = taskSingleMock;

  return makeTaskItem(item);
}

export const taskIndexAdapter = (items, isMock) => {
  if (isMock) items = taskIndexMock;
  return items.map(item => makeTaskItem(item))
}
export const makeTaskItem = (item) => {
  let modifiedItem = makeCommonItem(item);
  return {
    ...modifiedItem,
    isConfirmed: item.confirm,
    isCompleted: item.completed,
    balls: {
      current: roundStringNumberWithoutTrailingZeroes(item.balls?.current),
      total: roundStringNumberWithoutTrailingZeroes(item.balls?.total),
    },
    type: makeTypeOfTask(item.type),
    hasNotifications: item.notifications,
    attempts: item.attempts,
    canRepeatAfterFail: item.failed && item.number_attempt > item.attempts,
    attemptsAmount: item.number_attempt,
    group: item.group ? item.group.name : null,
    materials: makeFileItems(item.materials ?? item.files),
    isAvailableForExport: item.export?.availableExport,
    reports: item.export?.report,
    sessionUuid: item.export?.sessionUuid,
    btn: {
      ...makeTaskBtn(
        item
      ),
      loading: false
    }
  }
}
const makeTypeOfTask = (type) => {
  let label = '';
  if(type === TASK_TYPE_TEST) label = 'Тест';
  if(type === TASK_TYPE_EXPERT) label = 'Экс. оценка';
  if(type === TASK_TYPE_CONFIRMATION) label = 'Подтверждение';

  return {
    label,
    value: type
  }
};
const makeTaskBtn = (item) => {
  const hasAttempts = item.number_attempt > item.attempts;
  if(item.failed) {
    if(!hasAttempts || item.isExpired) {
      return {
        className: 'failed-disabled',
        label: 'Неуспешно',
        failed: true
      }
    }
    if(item.attempts > 0 && hasAttempts && !item.isExpired) {
      return {
        className: 'finished',
        label: 'Повторить'
      }
    }
  }

  // if(item.number_attempt !== VALUE_FOR_LIMITLESS_DEFINITION && item.number_attempt <= item.attempts) {
  //   return {
  //     className: 'finished',
  //     label: 'Попытки закончились'
  //   }
  // }


  if(item.confirm || item.completed) {
    return {
      className: 'finished',
      label: 'Пройдено'
    }
  }

  if (!item.open) {

    return {
      className: 'disabled',
      icon: 'lock',
      label: item.isExpired ? 'недоступно' : 'пока недоступно'
    }
  }

  if(item.open) {
    let label = '';

    if (item.type === TASK_TYPE_CONFIRMATION) {
      label = 'Отправить';
    } else if (item.continue) {
      label = 'Продолжить выполнение';
    } else {
      label = 'Начать выполнение';
    }

    return {
      className: 'opened',
      label: label
    }
  }

  return new Error('No Status Found');

}