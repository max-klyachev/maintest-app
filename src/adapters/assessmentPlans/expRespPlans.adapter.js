import { respExpPlanMock, respExpPlansMock } from '@/mocks/assessmentPlans/expRespPlans.mock';
import { getRecordsSortedByStatus, makeCommonItem } from '@/adapters/assessmentPlans/commonItem.adapter';
import { makeFileItem } from '@/adapters/files/files.adapter';
import { taskIndexAdapter } from '@/adapters/assessmentPlans/tasks/tasks.adapter';
import { roundStringNumberWithoutTrailingZeroes } from '@/services/Mathe.js';

export const respExpPlanSingleInputAdapter = (item, isMock) => {
  if (isMock) item = respExpPlanMock;
  return {
    ...makePlanItem(item),

    files: item.files ? item.files.map(file => makeFileItem(file)) : [],
    tests: item.tests ? getRecordsSortedByStatus(taskIndexAdapter(item.tests)) : [],
  };
}

export const respondentsAdapter = (respondents) => {
  return respondents ?
    respondents.map((respondent) => {
      return {
        uuid: respondent.uuid,
        fullName: respondent.full_name,
        age: respondent.year,
        reports: respondent.reports,
        position: respondent.position,
        grade: respondent.grade,
        totalScore: roundStringNumberWithoutTrailingZeroes(respondent.balls),
        tests: respondent.tests.map((task) => {
          let modifiedTask = {
            title: task.name,
            type: task.type,
            uuid: task.uuid,
            isOpen: false,
            isSet: task.completed,
            isFailed: task.failed,
            hasNotifications: task.notifications,
            reports: task.reports ?? [],
            sessionUuid: task.session_uuid,
            criterias: task.scales ? task.scales.map((scale) => {
              return {
                uuid: scale.uuid,
                name: scale.name,
                currentScore: roundStringNumberWithoutTrailingZeroes(scale.current),
                maxScore: scale.max
              }
            }) : [],
          };
          switch (task.type) {

            case 'confirmation':
              modifiedTask.isChecked = task.confirm;
              modifiedTask.icon = 'twoticks';
              break;
            case 'test':
              modifiedTask.totalScore = task.total;
              modifiedTask.currentScore = roundStringNumberWithoutTrailingZeroes(task.current);
              modifiedTask.icon = 'paper';

              break;
            case 'expert':
            case 'inspector':
              modifiedTask = Object.assign(modifiedTask, {
                currentScore: roundStringNumberWithoutTrailingZeroes(task.current),
                totalScore: task.total,
                totalMaxScore: roundStringNumberWithoutTrailingZeroes(task.totalMaxScore),
                isFailed: task.isFailed,
                isLocked: task.isLocked,
                icon: 'testexpert',
                hasNewMessages: task.notifications,
                sessionUuid: task.session_uuid,
              });
              break;
          }
          return modifiedTask;
        }).filter(task => !!task.title)
      }
    }) : [];
}

export const respondentPlansAdapter = respondents => {
  return respondents ?
      respondents.map((respondent) => {
        return {
          name: respondent.name,
          uuid: respondent.uuid,
          grade: respondent.grade,
          totalScore: roundStringNumberWithoutTrailingZeroes(respondent.balls),
          tests: respondent.tests.map((task) => {
            let modifiedTask = {
              title: task.name,
              type: task.type,
              uuid: task.uuid,
              isOpen: false,
              isSet: task.completed,
              isFailed: task.failed,
              hasNotifications: task.notifications,
              reports: task.reports ?? [],
              sessionUuid: task.session_uuid,
              criterias: task.scales ? task.scales.map((scale) => {
                return {
                  uuid: scale.uuid,
                  name: scale.name,
                  currentScore: roundStringNumberWithoutTrailingZeroes(scale.current),
                  maxScore: scale.max
                }
              }) : [],
            };
            switch (task.type) {

              case 'confirmation':
                modifiedTask.isChecked = task.confirm;
                modifiedTask.icon = 'twoticks';
                break;
              case 'test':
                modifiedTask.totalScore = task.total;
                modifiedTask.currentScore = roundStringNumberWithoutTrailingZeroes(task.current);
                modifiedTask.icon = 'paper';

                break;
              case 'expert':
              case 'inspector':
                modifiedTask = Object.assign(modifiedTask, {
                  currentScore: roundStringNumberWithoutTrailingZeroes(task.current),
                  totalScore: task.total,
                  totalMaxScore: roundStringNumberWithoutTrailingZeroes(task.totalMaxScore),
                  isFailed: task.isFailed,
                  isLocked: task.isLocked,
                  icon: 'testexpert',
                  hasNewMessages: task.notifications,
                  sessionUuid: task.session_uuid,
                });
                break;
            }
            return modifiedTask;
          }).filter(task => !!task.title)
        }
      }) : [];
}

export const respExpPlanIndexInputAdapter = (items, isMock) => {
  if (isMock) items = respExpPlansMock;
  return makePlanItems(items)
};

export const respExpItemsArchiveInputAdapter = (items, isMock) => {
  if (isMock) items = respExpPlansMock;
  return items.map(item => makePlanItem(item));
};

const makePlanItems = (items) => {
  return items.map(item => makePlanItem(item));
}
const makePlanItem = (item) => {
  let modifiedItem = makeCommonItem(item)
  return {
    ...modifiedItem,
    isCompleted: item.completed,
    progress: item.progress,
    participantsAmount: item.participants,
    score: roundStringNumberWithoutTrailingZeroes(item.balls?.current),
    totalScore: roundStringNumberWithoutTrailingZeroes(item.balls?.total),
    notifications: item.notifications
  }
};