export function generateId(length) {
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;

  for (let i = 0; i < length; i++) {
    result += characters.charAt(
      Math.floor(
        Math.random() * charactersLength
      )
    );
  }

  return result;
}
export function roundStringNumberWithoutTrailingZeroes (num, dp = 2) {
  if(!num) return 0;
  num = String(num);
  if (num.indexOf('e+') != -1) {
    // Can't round numbers this large because their string representation
    // contains an exponent, like 9.99e+37
    throw new Error("num too large");
  }
  if (num.indexOf('.') == -1) {
    // Nothing to do
    return num;
  }

  var parts = num.split('.'),
    beforePoint = parts[0],
    afterPoint = parts[1],
    shouldRoundUp = afterPoint[dp] >= 5,
    finalNumber;

  afterPoint = afterPoint.slice(0, dp);
  if (!shouldRoundUp) {
    finalNumber = beforePoint + '.' + afterPoint;
  } else if (/^9+$/.test(afterPoint)) {
    // If we need to round up a number like 1.9999, increment the integer
    // before the decimal point and discard the fractional part.
    finalNumber = Number(beforePoint)+1;
  } else {
    // Starting from the last digit, increment digits until we find one
    // that is not 9, then stop
    var i = dp-1;
    while (true) {
      if (afterPoint[i] == '9') {
        afterPoint = afterPoint.substr(0, i) +
          '0' +
          afterPoint.substr(i+1);
        i--;
      } else {
        afterPoint = afterPoint.substr(0, i) +
          (Number(afterPoint[i]) + 1) +
          afterPoint.substr(i+1);
        break;
      }
    }

    finalNumber = beforePoint + '.' + afterPoint;
  }

  // Remove trailing zeroes from fractional part before returning
  return finalNumber.replace(/0+$/, '')
}

export function formatBytes(bytes, decimals = 2) {
  if (!+bytes) return '0 Bytes'

  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}