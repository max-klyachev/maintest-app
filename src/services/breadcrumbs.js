import { te } from "date-fns/locale";

export default class {
  store = null;
  route = null;

  constructor({ store, }) {
    if (store) this.setStore(store);
  }

  /* SETTERS */
  setStore(store) {
    this.store = store;
  }

  /* ACTIONS */
  async setValue({ page, value, breadcrumbs, }) {
    if(!breadcrumbs) return [];

    return [
      ...breadcrumbs.map((breadcrumb) => {
        if (breadcrumb[page]) {
          return {
            ...breadcrumb,
            value: `${breadcrumb.value} ${value}`,
          };
        }

        return breadcrumb;
      })
    ];
  }
  async setQuerySettings({ value, breadcrumbs, query, params, }) {

    return [
      ...breadcrumbs.map((breadcrumb) => {
        if (breadcrumb.value === value) {
          return {
            ...breadcrumb,
            query,
            params,
          };
        }

        return breadcrumb;
      })
    ];
  }
  async init({ breadcrumbs, }) {

    await this.store._actions['breadcrumbs/setBreadcrumbs'][0]({ breadcrumbs, });

    // return this.store.getters['breadcrumbs/breadcrumbs'];
  }
}