export default new class {
  constructor() { }

  parseDate({ string, nodeSplitBy, itemSplitBy, divider, }) {
    if (!string) return "";

    const SPLITED_STRING = string.split(nodeSplitBy);
    const SPLITED_STRING_FIRST = SPLITED_STRING[0].split(itemSplitBy);


    
    if (SPLITED_STRING_FIRST.length < 3) {
      return string;
    }

    return `${SPLITED_STRING_FIRST[2]}${divider}${SPLITED_STRING_FIRST[1]}${divider}${SPLITED_STRING_FIRST[0]}`;
  }
  parseTime({ string, nodeSplitBy, }) {
    if (!string) return "";

    const splitedString = string.split(nodeSplitBy);

    return splitedString[1];
  }
}