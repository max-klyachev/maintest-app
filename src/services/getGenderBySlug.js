export default function (slug) {
  switch (slug) {
    case 'male':
      return 'мужской';

    case 'female':
      return 'женский';

    default:
      return '';
  }
}