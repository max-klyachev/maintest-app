import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export function formatDateTimeByDefault (date) {
  return formatDate(date, 'dd MMMM yyyy, HH:mm')
}

export function formatDateByDefault (date) {
  return formatDate(date, 'dd MMMM, yyyy')
}
export function formatDate (date, formatStr = 'PP') {
  if(!date) return null;

  return format(new Date(date), formatStr, {
    locale: ru
  })
}