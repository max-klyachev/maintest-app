import api from "@/network";

const AuthApi = {
  async selfRegister(payload) {
    return await api.post('/auth/self_registration', payload)
  },

  async getUserInfo() {
    return await api.get('/user')
  },

  async getUserAdditionalForm() {
    return await api.get('/user/form')
  },

  async updateUserInfo(payload) {
    return await api.put('/user/settings', payload)
  }
}

export default AuthApi