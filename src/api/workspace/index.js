import instance from "@/network";
import assessmentPlans from './assessmentPlans/assessmentPlans';
import support from './support/support-service';

export default {
  assessmentPlans : assessmentPlans( instance ),
  support : support( instance ),
}
