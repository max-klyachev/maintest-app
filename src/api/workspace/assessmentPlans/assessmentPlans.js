import planList       from './pages/PlanList';
import planListTable  from './pages/PlanListTable';
import addEdit1Attach from './pages/AddEdit1Attach';
import addEdit1Fields from './pages/AddEdit1Fields';
import addEdit4       from './pages/AddEdit4';
import addEdit2       from './pages/AddEdit2';
import expResp        from './pages/ExpResp';
import results        from './pages/Results';

export default function ( instance ) {
  return {
    planList        : planList( instance ),
    planListTable   : planListTable( instance ),
    addEdit1Attach  : addEdit1Attach( instance ),
    addEdit1Fields  : addEdit1Fields( instance ),
    addEdit4        : addEdit4( instance ),
    addEdit2        : addEdit2( instance ),
    expResp         : expResp( instance ),
    results         : results( instance ),
  }
}