export default function (instance) {
  return {
    getAccounts() { // DELETE
      return instance.get(`accounts`);
    },

    getUsers(uuid, name = '') { // DELETE
      return instance.get(`accounts/${uuid}/users?name=${name}`);
    },

    getUsersFromPlan(uuid, per_page = 5, page = 1, name = '', sort = null) { // DELETE
      let params = {
        data: JSON.stringify(
          {
            name: name,
            sort: sort,
            //filters   : filters,
          }
        )
      };

      return instance.get(`plans/${uuid}/participants?per_page=${per_page}&page=${page}`, { params: params });
    },

    async addUserToPlan(uuidPlan, uuidUser) { // DELETE
      let participant = {
        uuid: uuidUser,
      };

      return await instance.post(`plans/${uuidPlan}/participants`, participant);
    },

    deleteUserToPlan(uuidPlan, uuidUser) { // DELETE
      let participant = {
        uuid: uuidUser,
      };



      return instance.delete(`plans/${uuidPlan}/participants`, { data: participant });
    },
  }
}