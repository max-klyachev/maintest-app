export default function (instance) {
    return {
        sendPlansFile(uuid, file) {
            return instance.post(
                `plans/${uuid}/upload`,
                file,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            );
        },

        deleteFile(uuid) {
            return instance.delete(`files/${uuid}`);
        },
    }
}