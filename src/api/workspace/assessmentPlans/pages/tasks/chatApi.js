import api from '@/network';
import { chatMessagesMock } from '@/mocks/assessmentPlans/chatMessages.mock';

export default {
  async sendMessage (planUuid, taskUuid, sessionId, payload, isMock = false) {
    if(isMock) return chatMessagesMock[0];

    let formData = new FormData();
    formData.append('message', payload.message);
    payload.files.forEach((file) => {
      formData.append('file[]', file);
    })

    const response = await api.post(
      `/plans/${planUuid}/tests/${taskUuid}/session/${sessionId}/message`,
      formData
    );
    return response.data.data;
  },

  async getMessages (planUuid, taskUuid, sessionId, queryParams, isMock = false) {
    if(isMock) return chatMessagesMock;

    const response = await api.get(`/plans/${planUuid}/tests/${taskUuid}/session/${sessionId}/chat`, {
      params: queryParams
    });
    return response.data;
  }

}