import api from '@/network';

export default {
  async getScales (planUuid, taskUuid, sessionUuid) {
    const response = await api.get(`/plans/${planUuid}/tests/${taskUuid}/session/${sessionUuid}/scales`);
    return response.data.data;
  },

  async getMark (planUuid, taskUuid, sessionUuid) {
    const response = await api.get(`/plans/${planUuid}/tests/${taskUuid}/session/${sessionUuid}/result`);
    return response.data.data;
  },
  async setMark (planUuid, taskUuid, sessionUuid, payload, method = "post") {
    const response = await api({
      method: method,
      data: payload,
      url: `/plans/${planUuid}/tests/${taskUuid}/session/${sessionUuid}/result`
    });
    return response.data.data;
  },

  async getRespondentSession (planUuid, taskUuid, respondentUuid) {
    const response = await api.get(`/plans/${planUuid}/tests/${taskUuid}/respondent/${respondentUuid}`);
    return response.data;
  }
}