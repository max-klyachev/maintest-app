export default function ( instance ) {
  return {
    getInterpritations ( uuid, data )
    {
      return instance.put( `plans/${uuid}`, data );
    },

    createNewPlan ()
    {
      return instance.post( 'plans' );
    }
  }
}