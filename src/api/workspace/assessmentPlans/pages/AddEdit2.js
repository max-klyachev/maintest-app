export default function ( instance ) {
  return {
    async getTestScales ( uuid, uuidPlan )
    {


      try {
        return instance.get( `plans/${uuidPlan}/tests/${uuid}/scales` );
      } catch (e) {
        return e;
      }
    },
  }
}