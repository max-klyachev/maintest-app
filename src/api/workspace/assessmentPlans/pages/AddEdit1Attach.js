export default function (instance) {
  return {
    addNewAttachment(uuidPlan) {
      let data = {
        'uuid': uuidPlan,
      };

      return instance.post(`tests`, data);
    },

    updateAttachment(payload = {}) {

      return instance.put(`plans/${payload.uuidPlan}/tests/${payload.uuidTest}`, payload.test);
    },

    getAttachments() {
      return instance.get(`tests`);
    },

    getAttachment(uuid) {
      return instance.get(`tests/${uuid}`);
    },

    deleteAttachment(uuid) {
      return instance.delete(`tests/${uuid}`);
    },

    sendAttachmentFile(uuid, uuidPlan, file) {
      return instance.post(
        `plans/${uuidPlan}/tests/${uuid}/upload`,
        file,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }
      );
    },

    getServiceTests() {
      return instance.get(`service/tests`);
    },
  }
}