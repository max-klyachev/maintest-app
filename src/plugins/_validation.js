const validators = {
  "required": (payload = {}) => {
    if (payload.value !== null && payload.value !== undefined && payload.value.toString().length > 0) {
      return true;
    }

    return false;
  },
  "isEmail": (payload = {}) => {
    if (payload.value !== null && payload.value !== undefined) {
      const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

      return re.test(payload.value);
    }

    return false;
  },
  isNotEmpty: (payload = {}) => {
    return !!payload.value.length;
  },
  checkStep3PercentVal: (payload = {}) => {


    const interpritation = payload.store.planEditdataExd.interpritation;

    if (Number(payload.value)) {
      if (interpritation.length <= 1) {
        return true;
      }

      if (payload.index === 0) {
        return Number(interpritation[payload.index].to) < Number(interpritation[payload.index + 1].to);
      } else if (payload.index === interpritation.length - 1) {
        return Number(interpritation[payload.index].to) > Number(interpritation[payload.index - 1].to);
      } else {
        return Number(interpritation[payload.index].to) < Number(interpritation[payload.index + 1].to) &&
                Number(interpritation[payload.index].to) > Number(interpritation[payload.index - 1].to);
      }
    } else {
      return false;
    }
  },
  checkStep3PercentMax: (payload = {}) => {


    const interpritation = payload.store.planEditdataExd.interpritation;

    if (
      Number(payload.value)
        &&
      interpritation.length
    ) {

      if (payload.index === interpritation.length - 1) {
        return Number(interpritation[payload.index].to) === Number(payload.max);
      }

      return true;
    } else {
      return false;
    }
  },
}

export default {
  test (payload = {}) {
    let errorsPull = [];
    let valid = true;

    if (payload.rules.length) {
      payload.rules.forEach(r => {
        if (valid) {
          valid = validators[r.rule](payload.data);

          if (!valid) {
            errorsPull.push(r.message);
          }
        }
      });
    }

    return errorsPull.length > 0 ? errorsPull : [];
  },
  message (errorsPull) {
    const getMessage = (err) => {
      switch (err) {
        case "required":
          return "Необходимо заполнить"
          break;

        case "isEmail":
          return "Введите корректный email"
          break;
      }
    }
    return getMessage(errorsPull[0])
  },
}