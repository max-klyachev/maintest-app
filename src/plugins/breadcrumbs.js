import Breadcrumbs from '@/services/breadcrumbs';

export default {
  install: (App) => {
    App.config.globalProperties.$breadcrumbs = new Breadcrumbs({
      store: App.config.globalProperties.$store,
      // route: App.config.globalProperties.$route,
    });
  }
};