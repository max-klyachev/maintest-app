const validators = {
  "required": (value) => {
    // && value.toString().length > 0
    if (value !== null && value !== undefined && value !== '') return true
    return false
  },
  "isEmail": (value) => {
    if (value !== null && value !== undefined) {
      const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      return re.test(value)
    }
    return false
  },
  isNotEmpty: (value) => {
    return !!value.length
  },
  checkStep3PercentVal: (value) => {

    return true;
  },
}

export default {
  test(value, rules = []) {
    let errorsPull = []
    let valid = true

    if (rules.length > 0) {
      rules.forEach(r => {
        if (valid) {
          valid = validators[r.rule](value)
          if (!valid) errorsPull.push(r.message)
        }
      })
    }
    return errorsPull.length > 0 ? errorsPull : []
  },
  message(errorsPull) {
    const getMessage = (err) => {
      switch (err) {
        case "required":
          return "Необходимо заполнить"
          break;

        case "isEmail":
          return "Введите корректный email"
          break;
      }
    }
    return getMessage(errorsPull[0])
  }
}