import lodash from 'lodash';

export default {
  install: ( App ) => {
    App.config.globalProperties.$lodash = lodash;
  }
}