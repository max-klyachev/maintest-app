export default {
  install: ( App ) => {
    App.config.globalProperties.$helpersFunc = {
      has ( nodeList, element )
      {
        try {
          let classList = element.className.toString().split( ' ' );

          classList[ 0 ] = '.' + classList[ 0 ];

          let selector = classList.join( '.' );



          return selector !== '.' ? Array.from( nodeList ).filter( e => e.querySelector( selector ) ) : [];
        } catch (e) {
          return []
        }
        
      },
      formattedDate (timestamp) {
        let d = new Date(timestamp)
        let year = d.getFullYear().toString().substr(-2)
        let day = d.getDate()
        let month = d.getMonth() + 1
        let hours = d.getHours()
        let minuts  = d.getMinutes()

        const fmt = (i) => {
          if (i.toString().length === 1) return `0${i}`
          else return i
        }

        day = fmt(day)
        month = fmt(month)
        hours = fmt(hours)
        minuts = fmt(minuts)

        return `${day}.${month}.${year} ${hours}:${minuts}`
      }
    };
  }
}