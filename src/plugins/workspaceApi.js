import workspaceApi from '@/api/workspace/index';

export default {
  install: ( App ) => {
    App.config.globalProperties.$workspaceApi = workspaceApi;
  }
}