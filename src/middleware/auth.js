export default function auth({ next, nextMiddleware, to }) {


  const isAuth = localStorage.getItem('maintest_auth_data');

  if (!isAuth) {
    return next({
      name: 'Signin'
    })
  } else {
    let routes = [
      'Signup',
      'Signin',
      'Forgot'
    ]
    let has = routes.findIndex(route => route === to.name) + 1
    if (!!has) {
      return next('/')
    }
  }

  return nextMiddleware();
}