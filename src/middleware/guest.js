export default function guest ( { next, nextMiddleware } )
{


  const isAuth = localStorage.getItem( 'maintest_auth_data' );

  if( isAuth ) {
    return next({
      name: 'Index'
    })
  }

  return nextMiddleware();
}