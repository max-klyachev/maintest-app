import { mapActions, mapMutations, mapState } from "vuex";

export default {
    data: () => ({
        sort: {},
        search: {},
        respondentsLoading: false
    }),
    computed: {
        ...mapState('assessmentPlans/plan/task', ['mark', 'marksChanged']),
        ...mapState('assessmentPlans/plan', ['assessmentPlan', 'respondents', 'respondentsMeta', 'filters']),
    },
    methods: {
        ...mapActions('assessmentPlans/plan', ['fetchRespondents']),
        ...mapMutations('assessmentPlans/plan', ['updateFilters', 'updateStore']),

        async loadRespondents() {

            let params = {
                page: this.respondentsMeta.current_page,
                per_page: this.respondentsMeta.per_page,
                settings: {...this.sort}
            };
            if (this.filters.length) {
                params = {
                    ...params,
                    settings: {
                        ...params.settings,
                        filters: this.filters,
                    },
                };
            }

            // if (this.filterByResult) {
            //     params = {
            //         ...params,
            //         result: this.filterByResult,
            //     };
            // }

            if (!!this.search.value) {
                params.settings["value"] = this.search.value;

                if (!!this.search.search) {
                    params.settings["search"] = this.search.search;
                }
            }

            if(!this.marksChanged) this.respondentsLoading = true;

            await this.fetchRespondents({
                uuid: this.$route.params.planUuid,
                params: params
            })
                .finally(() => {
                    this.respondentsLoading = false;
                    this.updateStore({ entity: 'marksChanged', payload: false })
                });
        },
    },
    mounted() {
        this.loadRespondents()
    }
}