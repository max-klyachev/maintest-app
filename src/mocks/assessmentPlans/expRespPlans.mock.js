import { TASK_TYPE_CONFIRMATION, TASK_TYPE_EXPERT, TASK_TYPE_PRACTICE, TASK_TYPE_TEST } from '@/constants/testTypes';

export const respExpPlansMock = [
  {
    "uuid": "b074df3b-66c6-4e40-a5c8-369c92ddb069",
    "name": "калинин 02",
    "description": "в",
    "date_from": "2022-08-08 00:00:00",
    "date_to": "2022-08-08 00:00:00",
    "completed": false,
    "open": true,
    "progress": 0,
    "balls": {
      "total": 30,
      "current": 13
    }
  },
  {
    "uuid": "b074df32b-66c6-4e40-a5c8-369c92ddb069",
    "name": "OBEZBOL",
    "description": "hmmm",
    "date_from": "2022-08-08 00:00:00",
    "date_to": "2022-08-08 00:00:00",
    "completed": false,
    "open": true,
    "progress": 35,
    "balls": {
      "total": 50,
      "current": 25
    }
  },
  {
    "uuid": "4074df32b-66c6-4e40-a5c8-369c92ddb069",
    "name": "DUR'",
    "description": "hmmm",
    "date_from": "2022-08-09 00:00:00",
    "date_to": "2022-08-09 00:00:00",
    "completed": true,
    "open": false,
    "progress": 59,
    "balls": {
      "total": 50,
      "current": 25
    }
  }
]
export const respExpPlanMock = {
  "uuid": "b074df3b-66c6-4e40-a5c8-369c92ddb069",
  "name": "калинин 02",
  "description": "в",
  "date_from": "2022-08-08 00:00:00",
  "date_to": "2022-08-08 00:00:00",
  "completed": false,
  "open": true,
  "progress": 0,
  "balls": {
    "total": 30,
    "current": 13
  },
  "results_respondent": [
    {
      "full_name": "Иванов И.А.",
      "year": 19,
      "position": "Кассир",
      "tests": [
        {
          "title": "Согласие с ",
          "type": TASK_TYPE_CONFIRMATION,
          "confirm": false
        },
        {
          "title": "Знание основ ...",
          "type": TASK_TYPE_EXPERT,
          "total": 30,
          "current": 15,
          "criterias": [
            {
              "score": 5,
              "max_score": 10
            },
            {
              "score": 5,
              "max_score": 10
            },
            {
              "score": 5,
              "max_score": 10
            }
          ]
        },
        {
          "title": "Бизнес-профиль 8",
          "type": TASK_TYPE_TEST,
          "total": 30,
          "current": 15,
        },
        {
          "title": "Практическое задание",
          "type": TASK_TYPE_PRACTICE,
          "total": 30,
          "current": 15,
          "isUnread": true
        }
      ],
      "balls": 25,
      "grade": "Отлично"
    }
  ],
  "files": [
    {
      "uuid": "b074df3b-6666-4640-a5c8-369c92ddb069",
      "name": 'Reports.xls',
      "original_url": 'http://reports.ru',
      "extension": "xls",
      "size": 15555
    },
    {
      "uuid": "b074df3b-6666-4640-a5c8-369c92ddb069",
      "name": 'Reports.xls',
      "original_url": 'http://reports.ru',
      "extension": "xls",
      "size": 15555
    }
  ]
};