export const filesMock = [
  {
    "uuid": "78f71ecd-b9ea-4416-a9e4-ef0d6220d72e",
    "name": "Решение_о_предоставлении_услуги_ПЗК-20220904-7377294700-3",
    "file_name": "24a0bcbe-5f00-496b-be87-db15bc81ef24.pdf",
    "original_url": "https://maintest.infoshell.ru/storage/116/24a0bcbe-5f00-496b-be87-db15bc81ef24.pdf",
    "order": null,
    "extension": "pdf",
    "size": 93869
  },
  {
    "uuid": "a488afe0-5be3-4a64-a2b9-a71bad509baa",
    "name": "pomogat-prowe",
    "file_name": "24a0bcbe-5f00-496b-be87-db15bc81ef24.jpg",
    "original_url": "https://maintest.infoshell.ru/storage/123/24a0bcbe-5f00-496b-be87-db15bc81ef24.jpg",
    "order": null,
    "extension": "jpg",
    "size": 50252
  }
]