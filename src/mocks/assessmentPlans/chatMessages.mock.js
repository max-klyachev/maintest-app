import { filesMock } from '@/mocks/assessmentPlans/files.mock';

export const chatMessagesMock = [
  {
    "uuid": "b074df3b-66c6-4e40-a5c8-369c92ddb069",
    "message": "Waiting For The Worms",
    "files": filesMock,
    "user": {
      "uuid": "b0734df3b-66c6-4e40-a5c8-369c92ddb069",
      "role": "expert",
      "fullName": "Serj Tankyan"
    },
    "created_at": "2022-08-08 00:00:00",
  },
  {
    "uuid": "b074df3b-66c6-4e40-a5c8-369c92ddb069",
    "message": "Проверка сообщения , лампа не горит, врут календари, волна бежит на этот берег, нас как магнитом притянуло",
    "files": filesMock,
    "user": {
      "uuid": "b0734df3b-66c6-4e40-a5c8-369c92ddb069",
      "role": "expert",
      "fullName": "John Doe"
    },
    "created_at": "2022-08-08 00:00:00",
  }
]