export const planSettings = {
  selfRegistration: [
    {
      uuid: 'qwer-tyuio-asdf-ghjk1',
      title: 'Респондентам',
      status: false,
      link: 'sber.maintest.ru/plan.php?id=00112233'
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk2',
      title: 'Экспертам',
      status: true,
      link: 'sber.maintest.ru/plan.php?id=44556677'
    },
  ],
  notifications: [
    {
      uuid: 'qwer-tyuio-asdf-ghjk3',
      title: 'О результатах тестирования',
      status: true,
    },
  ],
  notificationSettings: [
    {
      uuid: 'qwer-tyuio-asdf-ghjk3',
      fullName: 'Иванов Гаянэ Александрович',
      role: 'методолог',
      status: true,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk4',
      fullName: 'Вишняков Мартын Иринеевич',
      role: 'администратор',
      status: true,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk5',
      fullName: 'Иванов Гаянэ Александрович',
      role: 'методолог',
      status: true,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk6',
      fullName: 'Вишняков Мартын Иринеевич',
      role: 'администратор',
      status: false,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk7',
      fullName: 'Иванов Гаянэ Александрович',
      role: 'методолог',
      status: false,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk8',
      fullName: 'Вишняков Мартын Иринеевич',
      role: 'администратор',
      status: false,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghjk9',
      fullName: 'Иванов Гаянэ Александрович',
      role: 'методолог',
      status: true,
    },
    {
      uuid: 'qwer-tyuio-asdf-ghj10',
      fullName: 'Вишняков Мартын Иринеевич',
      role: 'администратор',
      status: false,
    },
  ],
};
