import { filesMock } from '@/mocks/assessmentPlans/files.mock';

export const taskIndexMock = [
  {
    "uuid": "8c14cc0e-8061-4684-b556-4157f9445634",
    "name": "Marooned",
    "type": "test",
    "description": "До сих пор мне удавалось скрыть от моих товарищей, что я имею кое-что, но теперь они каким-то путем узнали об этом и под разными предлогами вытягивают у меня деньги.",
    "number_attempt": 1,
    "param_type": "auto",
    "date_from": "2022-08-31T21:00:00.000000Z",
    "date_to": "2022-09-29T21:00:00.000000Z",
    "completed": false,
    "open": true,
    "group": null,
    "materials": filesMock,
    "confirm": false,
    "attempts": 1
  },
  {
    "uuid": "8c14cc0e-8061-4684-b556-4157f9445634",
    "name": "High hopes",
    "type": "test",
    "description": "До сих пор мне удавалось скрыть от моих товарищей, что я имею кое-что, но теперь они каким-то путем узнали об этом и под разными предлогами вытягивают у меня деньги.",
    "number_attempt": 1,
    "param_type": "auto",
    "date_from": "2022-08-31T21:00:00.000000Z",
    "date_to": "2022-09-29T21:00:00.000000Z",
    "completed": false,
    "open": true,
    "group": null,
    "materials": filesMock,
    "confirm": false,
    "attempts": 1
  },
  {
    "uuid": "8c14cc0e-8061-4684-b556-4157f9445634",
    "name": "ТУР-7",
    "type": "test",
    "description": "До сих пор мне удавалось скрыть от моих товарищей, что я имею кое-что, но теперь они каким-то путем узнали об этом и под разными предлогами вытягивают у меня деньги.",
    "number_attempt": 1,
    "param_type": "auto",
    "date_from": "2022-08-31T21:00:00.000000Z",
    "date_to": "2022-09-29T21:00:00.000000Z",
    "completed": false,
    "open": true,
    "group": null,
    "materials": filesMock,
    "confirm": false,
    "attempts": 1
  }
]
export const taskSingleMock = {
  "uuid": "8c14cc0e-8061-4684-b556-4157f9445634",
  "name": "ТУР-7",
  "type": "test",
  "description": "До сих пор мне удавалось скрыть от моих товарищей, что я имею кое-что, но теперь они каким-то путем узнали об этом и под разными предлогами вытягивают у меня деньги.",
  "number_attempt": 1,
  "param_type": "auto",
  "date_from": "2022-08-31T21:00:00.000000Z",
  "date_to": "2022-09-29T21:00:00.000000Z",
  "completed": false,
  "open": true,
  "group": null,
  "materials": filesMock,
  "confirm": false,
  "attempts": 1
}