export const scales = [
  {
    uuid: 'ff-wdfwdf-wdf-wfw-d1f',
    name: 'Шкала 1',
  },
  {
    uuid: 'ff-wdfwdf-wdf-wfw-df2',
    name: 'Шкала 2',
  },
  {
    uuid: 'ff-wdfwdf-wdf-wfw-d3f',
    name: 'Шкала 3',
  },
  {
    uuid: 'ff-wdfwdf-wdf-wfw-d41f',
    name: 'Шкала 4',
  },
];