export const TASK_TYPE_TEST = 'test';
export const TASK_TYPE_EXPERT = 'expert';
export const TASK_TYPE_CONFIRMATION = 'confirmation';
export const TASK_TYPE_PRACTICE = 'practice';