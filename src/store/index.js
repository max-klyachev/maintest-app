import { createStore } from 'vuex';

import auth from './modules/auth/';
import sections from './modules/workspace/sections';
import dictionary from './modules/workspace/dictionary';
import assessmentPlans from './modules/workspace/assessmentPlans';
import testLibrary from './modules/workspace/testLibrary';
import administration from './modules/workspace/administration/index.js';
import results from './modules/workspace/results';
import test from './modules/workspace/test';
import report from './modules/workspace/report';
import breadcrumbs from './modules/workspace/breadcrumbs';

export default createStore(
  {
    modules: {
      auth,
      sections,
      dictionary,
      assessmentPlans,
      testLibrary,
      administration,
      results,
      test,
      report,
      breadcrumbs,
    }
  }
)
