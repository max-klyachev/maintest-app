import api from "@/network";

export default {
  namespaced: true,

  state: {
    settings: [],
  },

  getters: {
    settings(state) {
      return state.settings;
    }
  },

  actions: {
    /* FETCHERS */
    async fetchTestVariantBlock({ }, { uuidTest, uuidVariant, uuidBlock, }) {


      try {
        const response = await api.get(`tests/${uuidTest}/variants/${uuidVariant}/blocks/${uuidBlock}`);

        return response;
      } catch (e) {
        return e;
      }
    },

    /* API ACTIONS */
    async createTestVariantBlock({ }, { uuidTest, uuidVariant, payload, }) {


      try {
        const response = await api.post(
          `tests/${uuidTest}/variants/${uuidVariant}/blocks`,
          payload
        );

        return response;
      } catch (e) {
        return e;
      }
    },
    async updateTestVariantBlock({ }, { uuidTest, uuidVariant, uuidBlock, payload, }) {


      try {
        const response = await api.put(
          `tests/${uuidTest}/variants/${uuidVariant}/blocks/${uuidBlock}`,
          payload
        );

        return response;
      } catch (e) {
        return e;
      }
    },
    async updateTestVariantBlockMultiple({ }, { uuidTest, payload, }) {
      try {
        const response = await api.put(
          `tests/${uuidTest}/variants/blocks/mass-update`,
          payload.map((item) => ({
            uuid: item.uuid,
            order: item.order,
            status: item.status,
          }))
        );

        return response;
      } catch (e) {
        return e;
      }
    },
    async deleteTestVariantBlocks({ }, { uuidTest, payload, }) {
      try {
        const response = await api.delete(
          `tests/${uuidTest}/variants/blocks`,
          {
            data: payload.map(item => item.uuid),
          }
        );

        return response;
      } catch (e) {
        return e;
      }
    },

    /* LOCAL ACTIONS */
  },

  mutations: {
    async updateTestSettings(state, settings) {
      state.settings = settings;
    },
  },
}