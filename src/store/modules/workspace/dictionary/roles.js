import api from "@/network";


export default {
  state: {
    roles: [],
  },

  getters: {
    roles(state) {
      return state.roles;
    },
  },

  actions: {
    async fetchRoles({ commit }, payload = {}) {
      const response = await api.get(`dictionary/roles?type=${payload.type}`);

      commit('updateRoles', response?.data?.data);
      return response
    },

    async fetchAudience(context) {
      const response = await api.get('dictionary/audience')
      return response
    }
  },

  mutations: {
    updateRoles(state, roles) {
      state.roles = roles;
    },
  },
}