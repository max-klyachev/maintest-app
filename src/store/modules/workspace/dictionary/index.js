import api from "@/network";
import roles from './roles';

export default {
  state: {
    ...roles.state,
  },

  getters: {
    ...roles.getters,
  },

  actions: {
    ...roles.actions,

    async fetchMainTests() {
      const {data} = await api.get(`dictionary/mainTests`);
      return data
    },
    async fetchTags({ }, { search }) {
      const response = await api.get(
        `dictionary/tags`,
        { params: { search } }
      );

      return response
    },
    async fetchTestScales({ }, { uuidTest }) {
      const response = await api.get(
        `dictionary/mainTests/${uuidTest}/scales`,
      );

      return response;
    },
  },

  mutations: {
    ...roles.mutations,
  },
}