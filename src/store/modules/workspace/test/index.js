import api from "@/network";

export default {
  namespaced: true,

  state: {
    settings: [],
  },

  getters: {
    settings(state) {
      return state.settings;
    }
  },

  actions: {
    /* FETCHERS */
    async fetchTestSettings({ }, { uuidTest, }) {
      try {
        const response = await api.get(`tests/${uuidTest}/settings`);

        return response;
      } catch (e) {
        return e;
      }
    },

    /* API ACTIONS */
    async saveExportExcelSettings({ }, { uuidTest, uuidSettings, payload, }) {


      try {
        const response = await api.put(
          `tests/${uuidTest}/settings/${uuidSettings}`,
          payload
        );

        return response;
      } catch (e) {
        return e;
      }
    },

    /* LOCAL ACTIONS */
    async setTestsSettingsStore({ commit, getters, }, payload = null) {
      if (!payload) return;

      commit('updateTestSettings', {
        ...getters.settings,
        ...payload,
      });
    },
  },

  mutations: {
    async updateTestSettings(state, settings) {
      state.settings = settings;
    },
  },
}