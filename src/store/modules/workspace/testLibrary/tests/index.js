import api from "@/network";

/* ENTITIES */
import list from './list';
//import item from './item';

/* MODULES */

export default {
  namespaced: true,

  state: {
    ...list.state,
    // ...item.state,
  },
  getters: {
    ...list.getters,
    // ...item.getters,
  },
  actions: {
    ...list.actions,
    // ...item.actions,

    async runTest({}, payload = {}) {
      try {
        return await api.post(`tests/${payload.uuid}/sessions`);
      }
      catch (e) {
        return e;
      }
    },
    async updateTests({}, payload = {}) {
      try {
        return await api.put(
          `tests/active`,
          {
            tests: payload.tests,
            status: payload.status
          }
        );
      }
      catch (e) {
        return e;
      }
    },

    /* UPLOADERS */
    async addTags({ commit }, payload = {}) {
      try {
        return await api.put('tests/tags-attach', payload.tags);
      }
      catch (e) {
        return e;
      }
    },
  },
  mutations: {
    ...list.mutations,
    // ...item.mutations,
  },

  modules: {},
}