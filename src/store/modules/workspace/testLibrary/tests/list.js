import api from "@/network";

export default {
  state: {
    tests: [],
  },

  getters: {
    tests(state) {
      return state.tests;
    }
  },

  actions: {
    /* FETCHERS */
    async fetchTests({commit}, payload = {}) {
      try {
        const response = await api.get(`tests`, {
          params: {
            page: payload.page,
            per_page: payload.perpage,
            settings: payload.settings,
          },
        });



        //commit('updateTests', response.data.data);

        return response
      } catch (e) {
        return e;
      }
    },

    /* UPLOADERS */
    async addTags({commit}, payload = {}) {
      try {
        return await api.put('plans/tags-attach', payload.tags);
      }
      catch (e) {
        return e;
      }
    },
    async assignTestsToNewPlan({}, payload = {}) {


      try {
        return await api.post('tests/assign', payload.data);
      }
      catch (e) {
        return e;
      }
    },

    /* STORE ACTIONS */
    async resetTests({commit}) {
      commit('updatePlans', []);
    },

    /* API ACTIONS */
    async deleteTests({commit}, payload = {}) {
      try {
        return await api.delete('plans/delete', { data: payload.data });
      }
      catch (e) {
        return e;
      }
    },

    /* SETTERS */
    async switchTests({commit}, payload = {}) {
      try {
        return await api.put('plans/active', payload.plans);
      }
      catch (e) {
        return e;
      }
    },
  },

  mutations: {
    async updateTests(state, plans) {


      state.plans = plans;
    },
  },
}