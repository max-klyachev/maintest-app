import api from "@/network";

/* ENTITIES */

/* MODULES */

export default {
  namespaced: true,

  state: {},
  getters: {},
  actions: {
    /* FETCHERS */
    async fetchSessions({ commit }, payload = {}) {
      try {
        const response = await api.get(`sessions`, {
          params: {
            page: payload.page,
            per_page: payload.perpage,
            settings: payload.settings,
          },
        });



        //commit('updateTests', response.data.data);

        return response;
      } catch (e) {
        return e;
      }
    },

    /* ACTIONS */
    async setSessionsStatus({ commit }, { status, sessions, }) {


      try {
        const response = await api.put(`sessions/${status}`, sessions);



        return response
      } catch (e) {
        return e;
      }
    },
    async deleteSessions({ commit }, { sessions, }) {


      try {
        const response = await api.delete(
          `sessions/delete`,
          {
            params: sessions,
          }
        );



        return response;
      } catch (e) {
        return e;
      }
    },

    /* DOWNLOADERS */
    async exportSessions({ commit }, payload = {}) {
      try {
        const response = await api.get(`export/sessions`, {
          responseType: 'blob',
          params: {
            page: payload.page,
            per_page: payload.perpage,
            settings: payload.settings,
          },
        });



        //commit('updateTests', response.data.data);

        return response
      } catch (e) {
        return e;
      }
    },
  },
  mutations: {},
  modules: {},
}