import tests from './tests';
import sessions from './sessions';

export default {
  namespaced: true,

	state: {},
	getters: {},
	actions: {},
	mutations: {},

  modules: {
    tests,
    sessions,
  },
}