export default {
  namespaced: true,

  state: {
    breadcrumbs: [],
  },

  getters: {
    breadcrumbs(state) {
      return state.breadcrumbs;
    }
  },

  actions: {
    /* ACTIONS */
    async setBreadcrumbs({ commit }, { breadcrumbs, }) {
      commit('updateBreadcrumbs', breadcrumbs);
    },
  },

  mutations: {
    async updateBreadcrumbs(state, breadcrumbs) {
      state.breadcrumbs = breadcrumbs;
    },
  },
}