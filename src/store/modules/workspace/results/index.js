import api from "@/network";
import externalApi from "@/network/external";

export default {
  namespaced: true,

  state: {
    results: [],
  },

  getters: {
    results(state) {
      return state.results;
    }
  },

  actions: {
    /* FETCHERS */
    async fetchResults({ commit }, payload = {}) {
      try {
        const response = await api.get(`results`, {
          params: {
            page: payload.page,
            per_page: payload.perpage,
            settings: payload.settings,
          },
        });

        return response
      } catch (e) {
        return e;
      }
    },
    async fetchPlansForReplications({ }, { user, maintest, result, }) { // FIXME must move
      try {
        const response = await api.get(
          `results/plans`,
          {
            params: {
              user,
              maintest,
              result,
            },
          }
        );

        return response
      } catch (e) {
        return e;
      }
    },
    async fetchTrashResults({ commit }, payload = {}) {
      try {
        const response = await api.get(`results/trash`, {
          params: {
            page: payload.page,
            per_page: payload.perpage,
            settings: payload.settings,
          },
        });

        return response
      } catch (e) {
        return e;
      }
    },

    async addTags({ commit }, payload = {}) {
      try {
        return await api.put('results/tags-attach', payload.tags);
      }
      catch (e) {
        return e;
      }
    },
    async exportResultsReport({ }, payload) {


      try {
        const response = await api.post('export/results', payload);



        return response;
      }
      catch (e) {
        return e;
      }
    },
    async downloadResultsReport({ }, { reportLink, type, isGroup, }) {
      try {
        if (isGroup) {


          window.open(reportLink, "_blank");
        } else {


          const response = await externalApi.get(`${reportLink}`, {
            responseType: 'blob',
          });

          if (response.data.size) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');

            link.href = url;

            let fileName = `results__${new Date().toLocaleDateString()}-${new Date().toLocaleTimeString().replace(/:/g, '_')}.${type}`;

            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
          } else {
            this.$toast.error('Отчёт не найден')
          }
        }
      }
      catch (e) {
        return e;
      }
    },

    /* SERVICE ACTIONS */
    async deleteResults({ }, { results }) {
      try {
        const response = await api.delete('results', { data: results });

        return response;
      }
      catch (e) {
        return e;
      }
    },
    async deleteResultsForceAction({ }, { results }) {
      if (!results) return;

      try {
        const response = await api.delete('results/force-delete', { data: results });

        return response;
      }
      catch (e) {
        return e;
      }
    },
    async restoreResultsAction({ }, { results }) {
      if (!results) return;



      try {
        const response = await api.put('results/restore', results);

        return response;
      }
      catch (e) {
        return e;
      }
    },
    async reprocessResults({ }, { sessions }) {
      try {
        const response = await api.put('results/reprocess', { data: sessions });

        return response;
      }
      catch (e) {
        return e;
      }
    },
    async resultsPlansAttach({ }, { uuid, plans }) {
      try {
        const response = await api.put('results/plans-attach', [{ uuid, plans }]);

        return response;
      }
      catch (e) {
        return e;
      }
    },
  },

  mutations: {
    async updateResults(state, plans) {


      state.plans = plans;
    },
  },
}