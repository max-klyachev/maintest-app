export default {
	actions : {
		async setTitleActiveSection ( ctx, titleActiveSection ) {
			ctx.commit( 'updateTitleActiveSection', titleActiveSection );
		},

		async setAssessmentPlanName ( ctx, assessmentPlanName ) {
			ctx.commit( 'updateAssessmentPlanName', assessmentPlanName );
		},

		async setTitlesPath ( ctx, titlesPath ) {


			ctx.commit( 'updateTitlesPath', titlesPath );
		},
	},

	mutations : {
		updateTitleActiveSection ( state, titleActiveSection ) {
			state.titleActiveSection = titleActiveSection;
		},

		updateAssessmentPlanName ( state, assessmentPlanName ) {
			state.assessmentPlanName = assessmentPlanName;
		},

		updateTitlesPath ( state, titlesPath ) {
			state.titlesPath = titlesPath;
		},
	},

	state : {
		titleActiveSection : '',
		assessmentPlanName : '',
		titlesPath 				 : [
			{
				title : '',
				//routeName : 'AssessmentPlans'
			},
		],
	},

	getters : {
		titleActiveSection ( state ) {
			return state.titleActiveSection;
		},

		assessmentPlanName ( state ) {
			return state.assessmentPlanName;
		},

		titlesPath ( state ) {
			return state.titlesPath;
		},
	}
}