import api from "@/network";

export default {
	state	: {
		administrators : [],

		administratorsSortConfig  : {
      field	: null,
			order	: null,
    },
	},

	getters	: {
		administrators ( state )
		{
			return state.administrators;
		},

		administratorsSortConfig ( state )
    {
      return state.administratorsSortConfig;
    },
	},

	actions 	: {
    async importUsers (context, file) {
      let fd = new FormData()
      fd.append('file', file);
      let result = await api.put('import/users', fd, { headers: {
            'Content-Type': 'multipart/form-data'
          }})
      return result
    },


    async exportAdministrators (context, payload) {
      try {
        let url = 'export/users?type=admin'
        url = `${url}&settings=${JSON.stringify(payload.settings)}`
        const response = await api.get(url, { responseType: 'blob' });
        return response
      } catch (e) { return e }
    },

    async exportRespondents (context, payload) {
      try {
        let url = 'export/users?type=user'
        url = `${url}&settings=${JSON.stringify(payload.settings)}`
        const response = await api.get(url, { responseType: 'blob' });
        return response
      } catch (e) { return e }
    },
    async fetchAdministrators ( { commit } )
    {
      const response = await api.get( 'admin/user?type=admin' );

      commit( 'updateAdministrators', response?.data?.data );


    },
    async exportBilling (context, payload) {
      try {
        let url = 'export/billing'
        url = `${url}?settings=${JSON.stringify(payload.settings)}`
        const response = await api.get(url, { responseType: 'blob' });
        return response
      } catch (e) { return e }
    },

    /**
     *
     * @param { object } payload
     * {
     *    @param { string } uuid : "e0dde03b-becf-41f9-8e74-92fa224cb841",
     * }
     */
    // async deleteAdministrator ( { commit, state }, payload = {} )
    // {


    //   await api.delete( `admin/user/${ payload.uuid }` );

    //   let administrators = state.administrators.filter( ( administrator ) => { return administrator.uuid !== payload.uuid } );

    //   commit( 'updateAdministrators', administrators );
    // },

    async deleteAdministrator (context, uuid) {

      try {
        let res = await api.delete( `admin/user/${uuid}` );
        return res
      } catch (e) { return e }
    },
    async getAdministrators (context, payload) {

      try {
        let url = 'admin/user?type=admin'
        if (payload.page > 1) {
          url = `${url}&page=${payload.page}`

        }
        if (payload.perpage !== 0) {
          if (payload.page > 1) {
            url = `${url}&per_page=${payload.perpage}`
          } else {
            url = `${url}&per_page=${payload.perpage}`
          }
        }
        if (!!payload.settings) {
          url = `${url}&settings=${JSON.stringify(payload.settings)}`
        }
        const response = await api.get(url);
        return response
      } catch (e) { return e }
    }
	},

	mutations	: {
    async updateAdministrators ( state, administrators )
    {
      state.administrators = administrators;
    },
  },
}