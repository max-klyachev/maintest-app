import api from "@/network";

export default {
	state : {
    field : {
      variants : [],
    },
  },

	getters : {
    field ( state )
		{
			return state.field;
		},
  },

	actions : {
    async fetchField ( { commit }, payload = {} )
    {
      const response = await api.get( `admin/form/${ payload.uuid }` );

      commit( 'updateField', response?.data?.data );


    },

    async setFieldStatus ( { commit, state }, payload = {} )
    {
      const response = await api.put(
        `admin/form/${ payload.uuid }`,

        {
          ...state.field,

          status : payload.status,
        }
      );

      commit(
        'updateField',

        {
          ...state.field,

          status : payload.status,
        }
      );


    },

    async setFieldStore ( { commit, state }, payload = {} )
    {
      commit( 'updateField', { ...state.field, ...payload } );
    },

    async resetFieldStore ( { commit } )
    {
      commit(
        'updateField',

        {
          variants : [],
        } 
      );
    },

    async createField ( { commit, state } )
    {
      const response = await api.post( `admin/form`, state.field );



      commit( 'updateField', response?.data?.data );
    },

    async uploadField ( { state }, payload = {} )
    {


      return await api.put( `admin/form/${ payload.uuid }`, state.field );
    },
  },

	mutations : {
    async updateField ( state, field )
    {
      state.field = field;
    },
  },
}