import administrators from './administrators';
import administrator from './administrator';
import respondents from './respondents';
import respondent from './respondent';
import security from './security';
import branches from './branches';
import branch from './branch';
import fields from './fields';
import field from './field';
import user from './user';

export default {
	state : {
    ...administrators.state,
    ...administrator.state,
    ...respondents.state,
    ...respondent.state,
    ...security.state,
    ...branches.state,
    ...branch.state,
    ...fields.state,
    ...field.state,
    ...user.state
  },

	getters : {
    ...administrators.getters,
    ...administrator.getters,
    ...respondents.getters,
    ...respondent.getters,
    ...security.getters,
    ...branches.getters,
    ...branch.getters,
    ...fields.getters,
    ...field.getters,
    ...user.getters
  },

	actions : {
    ...administrators.actions,
    ...administrator.actions,
    ...respondents.actions,
    ...respondent.actions,
    ...security.actions,
    ...branches.actions,
    ...branch.actions,
    ...fields.actions,
    ...field.actions,
    ...user.actions
  },

	mutations : {
    ...administrators.mutations,
    ...administrator.mutations,
    ...respondents.mutations,
    ...respondent.mutations,
    ...security.mutations,
    ...branches.mutations,
    ...branch.mutations,
    ...fields.mutations,
    ...field.mutations,
    ...user.mutations
  },
}