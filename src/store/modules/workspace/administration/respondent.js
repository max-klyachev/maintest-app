import api from "@/network";

export default {
	state	: {
		respondent : {
      branches : [],
      formSettings : [],
    },
	},

	getters	: {
		respondent ( state )
		{
			return state.respondent;
		},

    respondentBranches ( state )
		{
			return state.respondent.branches || [];
		},
	},

	actions 	: {
    async fetchRespondent ( { commit }, payload = {} )
    {
      const response = await api.get( `admin/user/${ payload.uuid }` );



      commit( 'updateRespondent', response?.data?.data );
      return response
    },




    async setRespondentStore ( { commit, state }, payload = {} )
    {
      commit( 'updateRespondent', { ...state.respondent, ...payload } );
    },

    async addRespondentBranchStore ( { commit, state }, payload = {} )
    {
      let branches = state.respondent.branches;

      branches.push( payload );

      commit( 'updateRespondentBranches', branches );
    },

    async setRoleOfRespondentBranchStore ( { commit, state }, payload = {} )
    {


      let branches = state.respondent.branches;

      commit(
        'updateRespondentBranches',
        branches.map(
          ( branch ) => {
            if ( branch.branch.uuid === payload.branch.uuid )
            {

              return {
                ...branch,

                role : payload.role,
              }
            }

            return branch;
          }
        )
      );
    },

    async deleteRespondentBranchStore ( { commit, state }, payload = {} )
    {
      let branches = state.respondent.branches;

      commit(
        'updateRespondentBranches',

        branches.filter(
          ( branch ) => {
            return branch.branch.uuid !== payload.branch.uuid;
          }
        )
      );
    },

    async resetRespondentStore ( { commit } )
    {
      commit(
        'updateRespondent',

        {
          branches : [],
          formSettings : [],
        }
      );
    },

    async createRespondent ( { commit, state }, payload = false ) {
      try {
        let respondent = (!!payload) ? payload : state.respondent
        const response = await api.post( `admin/user`, respondent );
        commit( 'updateRespondent', response?.data?.data );
        return response
      } catch (e) {
        return e
      }
    },

    async uploadRespondent ( { state }, payload = {} ) {

      let data = (payload.respondent) ? payload.respondent : state.respondent
      return await api.put( `admin/user/${ payload.uuid }`, data );
    },
	},

	mutations	: {
    updateRespondent ( state, respondent )
    {
      state.respondent = respondent;
    },

    updateRespondentBranches ( state, branches )
    {
      state.respondent.branches = branches;
    }
  },
}