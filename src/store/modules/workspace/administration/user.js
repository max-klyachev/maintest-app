import api from "@/network";

export default {
  state : {
    userInfo: null
  },

  getters : {
    USER (state) {
      return state.userInfo
    },
    isMainAdmin (state) {
      // Если нет филиалов, то это админ главного офиса
      return state.userInfo?.branches.length === 0
    }
  },

  mutations : {
    updateUserInfo (state, payload) {
      state.userInfo = {
        ...payload
      }
    }
  },

  actions: {
    async fetchUserInfo ({ commit }) {
      const response = await api.get('head');
      commit('updateUserInfo', response.data.data );
      return response
    },


    async setAdminBranch ({ comit }, branchUuid) {
      try {
        const response = await api.put(`user/branch/${branchUuid}`);
      } catch (e) { console.log(e) }
    },

    async setUserStatus ({ commit }, { uuid, status }) { // TODO find
      try {
        const response = await api.put(`/admin/user/${uuid}/${status}`)
        return response
      } catch (e) { console.log(e) }
    }
  }
}