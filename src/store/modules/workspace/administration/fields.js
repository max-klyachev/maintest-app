import api from "@/network";

export default {
	state : {
    fields : [],
  },

	getters : {
    fields ( state )
		{
			return state.fields;
		},
  },

	actions : {
    async fetchFields ( { commit } ) {
      const response = await api.get( 'admin/form' );

      commit( 'updateFields', response?.data?.data );



      return response
    },

    async updateFields ({ commit }, fields) {
      const response = await api.put('admin/form', fields);
      return response
    },

    async deleteField ({ commit }, uuid) {
      const response = await api.delete( `admin/form/${uuid}` )
      return response
    },

    async deleteFieldsById ( { commit, state }, payload = {} ) // TODO refactoring just "uuid" name for attr
    {
      await api.delete( `admin/form/${ payload.uuid }` );

      commit(
        'updateFields',

        state.fields.filter(
          ( field ) => {
            return field.uuid !== payload.uuid;
          }
        )
      );
    },
  },

	mutations : {
    async updateFields ( state, fields )
    {
      state.fields = fields;
    },
  },
}