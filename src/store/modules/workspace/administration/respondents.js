import api from "@/network";

export default {
	state	: {
		respondents : [],
	},

	getters	: {
		respondents ( state )
		{
			return state.respondents;
		},
	},

	actions 	: {
    async fetchRespondents ( { commit } )
    {
      const response = await api.get( 'admin/user?type=user' );

      commit( 'updateRespondents', response?.data?.data );


    },
    async deleteRespondent (context, uuid) {
      try {
        let res = await api.delete( `admin/user/${uuid}` );
        return res
      } catch (e) { return e }
    },
    async getRespondents (context, payload) {


      try {
        let url = `admin/user?type=user`;

        if (payload.page > 1) {
          url = `${url}&page=${payload.page}`
        }
        if (payload.perpage !== 0) {
          if (payload.page > 1) {
            url = `${url}&per_page=${payload.perpage}`
          } else {
            url = `${url}&per_page=${payload.perpage}`
          }
        }
        if (!!payload.settings) {
          url = `${url}&settings=${JSON.stringify(payload.settings)}`
        }

        const response = await api.get(url);

        return response;
      } catch (e) {
        return e;
      }
    },

    /**
     * 
     * @param { object } payload
     * {
     *    @param { string } uuid : "e0dde03b-becf-41f9-8e74-92fa224cb841",
     * } 
     */
    async deleteRespondents ( { commit, state }, payload = {} )
    {


      await api.delete( `admin/user/${ payload.uuid }` );

      let respondents = state.respondents.filter( ( respondent ) => { return respondent.uuid !== payload.uuid } );

      commit( 'updateRespondents', respondents );
    },
	},

	mutations : {
		updateRespondents ( state, respondents )
    {
      state.respondents = respondents;
    },
	},
}