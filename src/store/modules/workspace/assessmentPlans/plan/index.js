/* ENTITIES */
import list from './list';
import item from './item';

/* MODULES */
import chat from './chat';
import task from './task';
import participant from './participant';

export default {
  namespaced: true,

	state : {
    ...list.state,
    ...item.state,
  },
	getters : {
    ...list.getters,
    ...item.getters,
  },
	actions : {
    ...list.actions,
    ...item.actions,
  },
	mutations : {
    ...list.mutations,
    ...item.mutations,
  },

  modules: {
    chat,
    task,
    participant
  },
}