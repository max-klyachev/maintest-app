import api from "@/network";
import { respExpPlanIndexInputAdapter } from '@/adapters/assessmentPlans/expRespPlans.adapter';

export default {
  state: {
    plans: [],
    expRespArchivePlans: [],
    expRespPlans: [],
    expRespActivePlans: {
      plans: []
    },
    expRespFinishedPlans: {
      plans: []
    },
  },
  getters: {
    archivePlansRows(state) {
      return [
        {
          title: '',
          plans: state.expRespArchivePlans
        }
      ]
    },
    assessmentPlans(state) {
      return state.plans;
    },
    plansRows(state) {
      return [
        {
          title: 'Список актуальных планов оценки:',
          plans: state.expRespActivePlans
        },
        {
          title: 'Список завершенных планов оценки:',
          plans: state.expRespFinishedPlans
        }
      ]
    },
  },

  actions: {
    /* FETCHERS */
    async fetchArchivePlans({ commit }, payload = {}) {
      const response = await api.get(
        'plans/mode/archive',
        {
          params: {
            page: payload.page ?? 1,
            per_page: payload.perpage ?? 30,
            settings: payload.settings,
            result: payload.result,
          },
        }
      );


      commit('updateArchiveExpRespPlans', response.data);
    },
    async fetchPlans({ commit }, payload = {}) {
      try {
        const response = await api.get(
          'plans',
          {
            params: {
              page: payload.page ?? 1,
              per_page: payload.perpage,
              settings: payload.settings,
              result: payload.result,
            },
          }
        );



        commit('updatePlans', response.data.data);

        return response
      }
      catch (e) {
        return e;
      }
    },

    async fetchActivePlans({ commit }, payload = {}) {
      const response = await api.get(
        'plans',
        {
          params: {
            page: payload.page ?? 1,
            per_page: payload.perpage ?? 30,
            settings: payload.settings,
            result: payload.result,
          },
        }
      );

      // commit('updatePlans', response.data.data);
      commit('updateExpRespActivePlans', response.data);
    },

    async fetchCompletedPlans({ commit }, payload = {}) {
      const response = await api.get(
        'plans/mode/completed',
        {
          params: {
            page: payload.page ?? 1,
            per_page: payload.perpage ?? 30,
            settings: payload.settings,
            result: payload.result,
          },
        }
      );

      commit('updateExpRespFinishedPlans', response.data);

      return response
    },

    /* UPLOADERS */
    async addTags({ commit }, payload = {}) {
      try {
        return await api.put('plans/tags-attach', payload.tags);
      }
      catch (e) {
        return e;
      }
    },

    /* STORE ACTIONS */
    async resetPlans({ commit }) {
      commit('updatePlans', []);
    },

    /* API ACTIONS */
    async deletePlans({ commit }, payload = {}) {
      try {
        return await api.delete('plans/delete', { data: payload.data });
      }
      catch (e) {
        return e;
      }
    },

    /* SETTERS */
    async switchPlans({ commit }, payload = {}) {
      try {
        return await api.put('plans/active', payload.plans);
      }
      catch (e) {
        return e;
      }
    },
    addToArchive({ commit }, payload = {}) {
      try {
        return api.put('plans/archive', payload.body);
      } catch (e) {
        return e;
      }
    },
    restorePlan({ commit }, payload = {}) {
      try {
        return api.put('plans/unarchive', payload.body);
      } catch (e) {
        return e;
      }
    },
  },

  mutations: {
    updateArchiveExpRespPlans(state, data) {
      state.expRespArchivePlans = {
        name: 'archive',
        plans: respExpPlanIndexInputAdapter(data.data),
        meta: data.meta
      };
    },
    updateExpRespPlans(state, plans) {
      state.expRespPlans = plans;
    },
    updateExpRespActivePlans(state, data) {
      state.expRespActivePlans.name = 'active'
      state.expRespActivePlans.meta = data.meta

      if (state.expRespActivePlans.plans.length && data.meta.current_page !== 1) state.expRespActivePlans.plans = state.expRespActivePlans.plans.concat(respExpPlanIndexInputAdapter(data.data))
      else state.expRespActivePlans.plans = respExpPlanIndexInputAdapter(data.data)
    },
    updateExpRespFinishedPlans(state, data) {
      state.expRespFinishedPlans.name = 'completed'
      state.expRespFinishedPlans.meta = data.meta

      if (state.expRespFinishedPlans.plans.length && data.meta.current_page !== 1) state.expRespFinishedPlans.plans = state.expRespFinishedPlans.plans.concat(respExpPlanIndexInputAdapter(data.data))
      else state.expRespFinishedPlans.plans = respExpPlanIndexInputAdapter(data.data)
    },
    clearExpRespPlans(state) {
      state.expRespFinishedPlans = {
        plans: []
      };
      state.expRespActivePlans = {
        plans: []
      };
    },
    async updatePlans(state, plans) {
      state.plans = plans;
    },
  },
}