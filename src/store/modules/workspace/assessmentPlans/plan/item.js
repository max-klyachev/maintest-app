import api from '@/network';
import { generateId, } from '@/services/Mathe';
import { respExpPlanSingleInputAdapter, respondentsAdapter } from '@/adapters/assessmentPlans/expRespPlans.adapter';
import useToast from "@/components/UI/AppToast/useToast";

export default {
  state: {
    respondents: [],
    respondentsMeta: {},
    assessmentPlan: {},
    filters: [],
    PlanEditdataExd: {
      interpritation: [],
      files: [],
    },
    PlanEditdataExdForCompare: null,
    tests: [],
    testEditdataExd: [],
    participants: [],
    results: [],
    columnsPlans: [],
    columnsParticipants: [],
    selectedPlans: {},
    selectedPlanTags: [],
    date_from: false,
    date_to: false,
    addEdit1Validation: {
      invalidName: false,
      invalidDesc: false,
      invalidAccessTime: false,
      validationError: false,
    },
    addEdit2Validation: {
      errorWeight: false,
      errorLimit: false,
      errorMax: false,
      errorMin: false,
      errorStep: false,
    },
    planListSortConfig: {
      field: null,
      order: null,
    },
    participantsSortConfig: {
      field: null,
      order: null,
    },
    resultsSortConfig: {
      field: null,
      order: null,
    },
    settings: {},
    filesToAdd: [],
    filesToDelete: [],
    isNewPlan: false
  },

  getters: {
    rows(state) {

      return [
        {
          title: 'Актуальные задания',
          items: state.assessmentPlan?.tests?.active
        },
        {
          title: 'Выполненные задания',
          items: state.assessmentPlan?.tests?.finished
        }
      ]
    },
    planEditdataExd(state) {
      return state.PlanEditdataExd;
    },
    date_from(state) {
      return state.date_from;
    },
    date_to(state) {
      return state.date_to;
    },
    tests(state) {
      return state.PlanEditdataExd.structure ?? [];
    },
    testEditdataExd(state) {
      return state.testEditdataExd;
    },
    participants(state) {
      return state.participants;
    },
    interpritation(state) {
      return state.PlanEditdataExd.interpritation ?? [];
    },
    addEdit1Validation(state) {
      return state.addEdit1Validation;
    },
    addEdit2Validation(state) {
      return state.addEdit2Validation;
    },
    participantsSortConfig(state) {
      return state.participantsSortConfig;
    },
    columnsParticipants(state) {
      return state.columnsParticipants;
    },
    results(state) {
      return state.results;
    },
    resultsSortConfig(state) {
      return state.resultsSortConfig;
    },
    settings(state) {
      return state.settings;
    },
    files(state) {
      return [
        ...state.PlanEditdataExd.files.map(
            (file) => ({
              ...file,
              url: file.original_url,
            })
        ).filter(
            file => !state.filesToDelete.filter(
                fileToDelete => fileToDelete === file.uuid
            ).length
        ),
        ...state.filesToAdd.map(
          (file) => ({ ...file.node })
        ),
      ];
    },
    filesToAdd(state) {
      return state.filesToAdd;
    },
    filesToDelete(state) {
      return state.filesToDelete;
    }
  },

  actions: {
    /* FETCHERS */
    async fetchPlan({ commit }, payload = {}) {
      const response = await api.get(`plans/${payload.uuid}`, {params: payload.params ? payload.params : null });
      const data = response.data.data;

      if(data) {
        commit('updatePlanStore', data);
        commit('updateExpRespAssessmentPlan', {...respExpPlanSingleInputAdapter(data), meta: response.data.meta});
        return data
      }
      commit('updatePlanStore', response.data);
      commit('updateExpRespAssessmentPlan', response.data);
    },
    async fetchRespondents({ commit }, payload = {}) {
      const response = await api.get(`plans/${payload.uuid}/respondents`, {params: payload.params ?? null });
      const respondents = response.data.data;
      commit('updateRespondents', {respondents: respondentsAdapter(respondents), meta: response.data.meta});
    },
    async completePlan({ commit }, planUuid) {
      await api.put(`/plans/${planUuid}/completed`).then(() => {
        commit('completePlan')
      });
    },
    async resumePlan({commit}, planUuid) {
      await api.put(`/plans/${planUuid}/resume`).then(() => {
        commit('completePlan', false)
      });
    },
    async fetchPlanSettings({ }, { uuid, }) {
      try {
        const response = await api.get(`plans/${uuid}/settings`);

        return response.data.data;
      } catch (e) {
        return e;
      }
    },
    async fetchPlanSettingsNotifications({ }, { uuid, search, }) {
      let params = {};

      if (search) {
        params = {
          ...params,
          settings: {
            value: search,
          },
        };
      }

      try {
        const response = await api.get(`plans/${uuid}/user-notifications`, { params, });

        return response.data.data;
      } catch (e) {
        return e;
      }
    },

    /* API ACTIONS */
    async updatePlanSettings({ }, { uuid, payload, }) {
      try {
        const response = await api.put(`plans/${uuid}/settings`, payload);

        return response.data.data;
      } catch (e) {
        return e;
      }
    },

    async enableSelfRegistrationLink({}, {uuid}) {
      try {
        const { data } = await api.put(`plans/self_registration/enable`, {uuid});

        return data;
      } catch (e) {
        return e;
      }
    },

    async disableSelfRegistrationLink({}, {uuid}) {
      try {
        const { data } = await api.put(`plans/self_registration/disable`, {uuid});

        return data;
      } catch (e) {
        return e;
      }
    },

    async updateSelfRegistrationLink({ state }, {uuid}) {
      try {
        const { data } = await api.put(`plans/self_registration/link_update`, {
          uuid
        });

        Object
            .keys(state.settings.selfRegistration)
            .forEach(key => {
              if(state.settings.selfRegistration[key].uuid === data.uuid) {
                state.settings.selfRegistration[key].url = data.url
              }
            })

        return data;
      } catch (e) {
        return e;
      }
    },

    async updatePlanSettingsNotifications({ getters, }, { uuid, }) {
      const USERS_TO_UPDATE = getters.settings
        .notificationSettings
        .filter(user => user.status)
        .map(user => user.uuid);

      try {
        const response = await api.post(`plans/${uuid}/user-notifications`, USERS_TO_UPDATE);

        return response.data.data;
      } catch (e) {
        return e;
      }
    },
    async addFile({ }, { uuid, file, }) {

      if (!uuid && !file) return;

      try {
        const response = await api.post(
          `plans/${uuid}/upload`,
          file,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          }
        );

        return response;
      } catch (e) {
        return e;
      }
    },
    async deleteFile({ }, { uuid, }) {


      if (!uuid) return;

      try {
        const response = await api.delete(`files/${uuid}`);

        return response;
      } catch (e) {
        return e;
      }
    },

    /* UPLOADERS */
    async createPlan({ }, payload = {}) {
      try {
        return await api.post(`plans`);
      } catch (e) {
        return e;
      }
    },
    async deletePlan({ }, payload = {}) {
      try {
        return await api.delete(`plans/${payload.uuid}`);
      } catch (e) {
        return e;
      }
    },
    async updatePlan({ }, payload = {}) {
      try {
        return await api.put(`plans/${payload.plan.uuid}`, payload.plan);
      } catch (e) {
        return e;
      }
    },

    /* SETTERS STORE */
    async setPlanStore({ commit, state }, payload = {}) {
      commit('updateStore', {
        entity: 'PlanEditdataExd',
        payload: {
          ...state.PlanEditdataExd,
          ...payload,
        },
      });
    },
    async resetPlanStore({ commit, state }, payload = {}) {
      commit('updatePlanStore', {
        interpritation: [],
        files: [],
      });
      commit('updateStore', {
        entity: 'filesToAdd',
        payload: [],
      });
      commit('updateStore', {
        entity: 'filesToDelete',
        payload: [],
      });
    },
    async setPlanSettingsStore({ commit, getters, }, payload = null) {
      if (!payload) return;

      commit('updatePlanSettings', {
        ...getters.settings,
        ...payload,
      });
    },

    async setFileToAdd({ commit, state}, { file }) {
      const {toast} = useToast()
      if(file.node.size > 10000000) return toast.error('Размер файла не должен превышать 10мб')

      commit('updateStore', {
        entity: 'filesToAdd',
        payload: [
          ...state.filesToAdd,
          {
            ...file,
            node: {
              uuidLocal: generateId(25),
              name: file.node.name
            }
          },
        ],
      });
    },
    async unsetFileToAdd({ commit, state, }, { file, }) {
      commit('updateStore', {
        entity: 'filesToAdd',
        payload: state.filesToAdd.filter(
          fileToAdd => fileToAdd.node.uuidLocal !== file.uuidLocal
        ),
      });
    },
    async setFileToDelete({ commit, state, }, { uuid, }) {

      commit('updateStore', {
        entity: 'filesToDelete',
        payload: [
          ...state.filesToDelete,
          uuid,
        ],
      });
    },

    /* ACTIONS */
    async planGroupsList(context, planUuid) {
      const response = await api.get(`plans/${planUuid}/groups`);
      return response
    },
    async createPlanGroup(context, { planUuid, groupData }) {
      const response = await api.post(`plans/${planUuid}/groups`, groupData); // DELETE

      return response
    },
    async removeGroup(context, { planUuid, groupUuid }) {
      const response = await api.delete(`plans/${planUuid}/groups/${groupUuid}`); // DELETE

      return response
    },
    async setPlanEditdataExd(ctx, PlanEditdataExd) {
      ctx.commit('updatePlanEditdataExd', PlanEditdataExd);
    },
    async setTestEditdataExd(ctx, testEditdataExd) {
      ctx.commit('updateTestEditdataExd', testEditdataExd);
    },
    async set_date_from(ctx, date_from) {
      ctx.commit('update_date_from', date_from);
    },
    async set_date_to(ctx, date_to) {
      ctx.commit('update_date_to', date_to);
    },

    /* TASKS */
    setTask({ commit }, payload = {}) {
      commit('updateTasks', payload);
    },
    updateSingleTask ({commit}, payload = {}) {
      commit('updateTask', payload)
    },
    async createTask({ commit }, payload = {}) {
      return await api.post(`plans/${payload.uuid}/tests`, payload);
    },
    async updateTask({ commit }, payload = {}) {
      return api.put(`plans/${payload.uuid}/tests/${payload.task.uuid}`, payload.task);
    },
    async deleteTask({ commit }, payload = {}) {
      return await api.delete(`plans/${payload.uuidPlan}/tests/${payload.uuidTest}`);
    },
  },

  mutations: {
    completePlan (state, val = true) {
      state.assessmentPlan.isCompleted = val;
    },
    moveTestToFinished (state, test) {
      test.isConfirmed = true;
      test.isCompleted = true;
      test.btn.label = 'Пройдено';
      state.assessmentPlan.tests.active = state.assessmentPlan.tests.active.filter(item => item.uuid !== test.uuid);
      state.assessmentPlan.tests.finished.unshift(test);
    },
    updateExpRespAssessmentPlan (state, data) {
      state.assessmentPlan = data;
    },
    updatePlanStore(state, payload = {}) {
      state.PlanEditdataExd = {
        ...payload,
        files_expert: []
      };
      if(state.PlanEditdataExd?.structure) {
        state.PlanEditdataExd.structure = state.PlanEditdataExd.structure.map(task => {
          if(task.materials.length) task.files = task.materials
          return task
        })
      }

      if(!state.PlanEditdataExdForCompare) {
        state.PlanEditdataExdForCompare = JSON.parse(JSON.stringify(state.PlanEditdataExd));
      }
    },
    updatePlanEditdataExd(state, PlanEditdataExd) {
      state.PlanEditdataExd = PlanEditdataExd;
    },
    updateTests(state, tests) {
      state.PlanEditdataExd.structure = tests;
    },
    updateFilters(state, filters) {
      state.filters = filters;
    },
    updateRespondents(state, { respondents, meta }) {
      state.respondents = respondents;
      state.respondentsMeta = meta;
    },
    updateParticipants(state, participants) {
      state.participants = participants;
    },
    updateResults(state, results) {
      state.results = results;
    },
    updatePlanSettings(state, settings) {
      state.settings = settings;
    },
    updateStore(state, { entity, payload, }) {
      state[entity] = payload;
    },
    updateTasks(state, payload) {
      state.PlanEditdataExd = {
        ...state.PlanEditdataExd,
        structure: [
          ...state.PlanEditdataExd.structure,
          payload,
        ]
      }
    },
    updateTask(state, payload) {
      state.PlanEditdataExd.structure = state.PlanEditdataExd.structure.map(item => {
        if(item.uuid === payload.uuid) {
          Object.keys(payload).forEach(key => {
            item[key] = payload[key]
          })
        }
        return item
      })
    },
    updateIsNewPlan(state, payload) {
      state.isNewPlan = payload
    }
  },
}