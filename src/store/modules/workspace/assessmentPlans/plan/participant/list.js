import api from "@/network";

export default {
  state: {
    participants: {},
  },

  getters: {
    participants(state) {
      return state.participants;
    }
  },

  actions: {
    /* FETCHERS */
    async fetchParticipants({ commit }, payload = {}) {


      try {
        const response = await api.get(
          `plans/${payload.uuid}/participants`,
          {
            params: {
              page: payload.params.page,
              per_page: payload.params.perpage,
              settings: payload.params.settings,
            },
          }
        );

        commit('updateParticipants', response.data);

        return response;
      } catch (e) {
        return e;
      }
    },
    async fetchAllParticipants({ }, { uuid, params, }) {


      try {
        return await api.get(
          `plans/${uuid}/users`,
          // `accounts/users`,
          {
            params: {
              page: params.page,
              per_page: params.perpage,
              settings: params.settings,
            },
          }
        )
      } catch (e) {
        return e;
      }
    },

    /* SETTERS STORE */
    async setParticipantsStore({ commit, state }, payload = {}) {
      commit('updateParticipants', {
        ...state.participants,
        ...payload,
      });
    },
  },

  mutations: {
    async updateParticipants(state, participants) {
      state.participants = participants;
    },
  },
}