import api from "@/network";
import list from './list';
import item from './item';

export default {
  namespaced: true,

  state: {
    ...list.state,
    ...item.state,
  },

  getters: {
    ...list.getters,
    ...item.getters,
  },

  actions: {
    ...list.actions,
    ...item.actions,

    /* API ACTIONS */
    /**
     * @param {object} payload: {
     * @param {string} uuidPlan,
     * @param {array} data: [
          '67f506ff-1fc9-4e27-b845-0891a06dbd61',
        ],
     * }
     */
    async deleteParticipants({commit}, payload = {}) {


      try {
        return await api.delete(
          `plans/${payload.uuidPlan}/participants/delete`,
          {
            data: payload.data,
          }
        );
      } catch (e) {
        return e;
      }
    },
    async addParticipantToPlan({}, payload = {}) {

      return await api.post(
        `plans/${payload.uuidPlan}/participants`,
        {
          uuid: payload.uuidParticipant,
        }
      );
    },
  },

  mutations: {
    ...list.mutations,
    ...item.mutations,
  },
}