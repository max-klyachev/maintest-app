import chatApi from '@/api/workspace/assessmentPlans/pages/tasks/chatApi';
import {
  chatMessageIndexInputAdapter,
  chatMessageSingleInputAdapter
} from '@/adapters/assessmentPlans/tasks/messages/chatMessageIndexInputAdapter';

export default {
  namespaced: true,

	actions: {
    async getMessages ({commit}, { planUuid, taskUuid, sessionId, queryParams, isMock }) {
      const messages = await chatApi.getMessages(planUuid, taskUuid, sessionId, queryParams, isMock);
      commit('setChatMessages', {
        messages: chatMessageIndexInputAdapter(messages.data),
        meta: messages.meta
      });
    },
    async sendMessage ({commit}, {planUuid, taskUuid, sessionUuid, payload }) {
      let newMessage = await chatApi.sendMessage(planUuid, taskUuid, sessionUuid, payload);
      commit('addNewMessage', chatMessageSingleInputAdapter(newMessage));
    }
  },

	mutations: {
    setChatMessages(state, { messages, meta }) {
      // if(meta.current_page === 1) state.chatMessages = messages.reverse();
      // else state.chatMessages.push()
      state.chatMessages = [...messages.reverse(), ...state.chatMessages]
      state.meta = meta
    },
    addNewMessage(state, message) {
      state.chatMessages.push(message);
      state.messageCounter = state.messageCounter + 1;
    },
    updateMessageCounter(state) {
      state.messageCounter = state.messageCounter + 1;
    }
  },

  state: {
    messageCounter: 0,
    chatMessages: [],
    meta: {}
  }
}