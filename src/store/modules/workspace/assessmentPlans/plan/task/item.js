import api from "@/network";
import { generateId, } from '@/services/Mathe';
import { taskSingleAdapter } from '@/adapters/assessmentPlans/tasks/tasks.adapter';
import expertCustomAssessmentApi from '@/api/workspace/assessmentPlans/pages/tasks/expertCustomAssessmentApi';
import { makeMarkItem, scaleIndexAdapter } from '@/adapters/assessmentPlans/tasks/customAssessment.adapter';
import useToast from "@/components/UI/AppToast/useToast";

const fetchBasicTask = async (payload) => {
    try {
        return await api.get(`plans/${ payload.uuidPlan }/tests/${ payload.uuid }`);
    } catch (e) {

        return e;
    }
}

export default {
    state: {
        mark: null,
        sessionUuid: '',
        expRespTask: null,
        task: {
            parameters: {
                param_type: ''
            },
            files: [],
            files_expert: [],
            mainTest: {
                TestTitle: ''
            }
        },
        filesToAdd: [],
        filesToAddExpert: [],
        filesToDelete: [],
        filesExpertToDelete: [],

        messages: [],

        scales: [],

        marksChanged: false
    },

    getters: {
        task(state) {
            return state.task;
        },
        files(state) {
            return [
                ...state.task.files.map(
                    (file) => ({
                        ...file,
                        url: file.original_url,
                    })
                ).filter(
                    file => !state.filesToDelete.filter(
                        fileToDelete => fileToDelete === file.uuid
                    ).length
                ),
                ...state.filesToAdd.map(
                    (file) => ({ ...file.node })
                )
            ];
        },
        filesExpert(state) {
            return [
                ...state.task.files_expert
                    .map(
                        (file) => ({
                            ...file,
                            url: file.original_url,
                        })
                    ).filter(
                        file => !state.filesExpertToDelete.filter(
                            fileToDelete => fileToDelete === file.uuid
                        ).length
                    ),
                ...state.filesToAddExpert.map(
                    (file) => ({ ...file.node })
                )
            ];
        },
        filesToAdd(state) {
            return state.filesToAdd;
        },
        filesToAddExpert(state) {
            return state.filesToAddExpert;
        },
        filesToDelete(state) {
            return state.filesToDelete;
        },
        filesExpertToDelete(state) {
            return state.filesExpertToDelete;
        }
    },

    actions: {
        async getMark({commit}, {planUuid, taskUuid, sessionUuid}) {
            const mark = await expertCustomAssessmentApi.getMark(planUuid, taskUuid, sessionUuid);
            commit('updateMark', makeMarkItem(mark));
        },
        async setMark({commit}, {planUuid, taskUuid, sessionUuid, payload, method = 'post'}) {
            const newMark = await expertCustomAssessmentApi.setMark(planUuid, taskUuid, sessionUuid, payload, method);
            /*commit('updateMark', makeMarkItem(newMark));*/
        },

        async getUserSession({commit}, {planUuid, taskUuid, respondentUuid}) {
            const sessionId = await expertCustomAssessmentApi.getRespondentSession(planUuid, taskUuid, respondentUuid);
            commit('updateSessionUuid', sessionId);
        },

        /* FETCHERS */
        async getScales({commit}, {planUuid, taskUuid, sessionUuid}) {
            const scales = await expertCustomAssessmentApi.getScales(planUuid, taskUuid, sessionUuid);
            commit('updateScales', scaleIndexAdapter(scales));
        },
        async fetchExpRespTask({commit}, payload = {}) {
            const response = await fetchBasicTask(payload);
            const data = taskSingleAdapter(response.data.data, payload.isMock);
            commit('updateExpRespTask', data);
        },

        async fetchTask({commit}, payload = {}) {
            const response = await fetchBasicTask(payload);
            commit('updateStore', {
                entity: 'task',
                payload: response.data.data,
            });
            return response;
        },

        /* SETTERS STORE */
        async setTaskStore({commit, state}, payload = {}) {
            commit('updateStore', {
                entity: 'task',
                payload: {
                    ...state.task,
                    ...payload,
                },
            });
        },
        async resetTaskStore({commit, state}, payload = {}) {
            commit('updateStore', {
                entity: 'task',
                payload: {
                    files: [],
                    files_expert: [],
                    parameters: {
                        param_type: ''
                    },
                    mainTest: {
                        TestTitle: ''
                    }
                },
            });
            commit('updateStore', {
                entity: 'filesToAdd',
                payload: [],
            });
            commit('updateStore', {
                entity: 'filesToDelete',
                payload: [],
            });
            commit('updateStore', {
                entity: 'filesExpertToDelete',
                payload: [],
            });
        },
        async setFileToAdd({ commit, state, }, { file, entity }) {
            const { toast } = useToast()
            if (file.node.size > 10000000) return toast.error('Размер файла не должен превышать 10мб')

            commit('updateStore', {
                entity,
                payload: [
                    ...state[entity],
                    {
                        ...file,
                        node: {
                            uuidLocal: generateId(25),
                            name: file.node.name,
                        }
                    },
                ],
            });
        },
        async unsetFileToAdd({commit, state,}, {file,}) {
            commit('updateStore', {
                entity: 'filesToAdd',
                payload: state.filesToAdd.filter(
                    fileToAdd => fileToAdd.node.uuidLocal !== file.uuidLocal
                ),
            });
        },
        async setFileToDelete({commit, state,}, {uuid, entity = 'filesToDelete'}) {
            commit('updateStore', {
                entity,
                payload: [
                    ...state[entity],
                    uuid,
                ],
            });
        },

        /* ACTIONS */

        /* ACTIONS API */
        async startTaskSession({}, {planUuid, taskUuid}) {
            return await api.post(`plans/${planUuid}/tests/${taskUuid}/session`);
        },
        async startCustomAssessmentSession({commit}, {planUuid, taskUuid}) {
            return await api.post(`plans/${planUuid}/tests/${taskUuid}/session/expert-review`)
                .then((response) => {
                    const sessionId = response.data.uuid;
                    commit('updateSessionUuid', sessionId);
                });
        },
        async confirmTask({commit}, {planUuid, test}) {
            return await api.post(`plans/${planUuid}/tests/${test.uuid}/confirm`)
                .then(() => {
                    commit('assessmentPlans/plan/moveTestToFinished', test, {root: true});
                });
        },
        async updateTask({ commit }, payload = {}) {
            try {
                const response = await api.put(`plans/${ payload.uuidPlan }/tests/${ payload.uuidTest }`, payload.data);

                return response;
            } catch (e) {
                return e;
            }
        },
        async addFile({}, { uuidPlan, uuidTask, file, type = null }) {
            if (!uuidPlan && !uuidTask && !file) return;

            try {
                const headers = {
                    'Content-Type': 'multipart/form-data',
                }
                const url = type !== 'expert' ? `plans/${ uuidPlan }/tests/${ uuidTask }/upload` : `plans/${ uuidPlan }/tests/${ uuidTask }/upload_expert`
                const response = await api.post(url, file, { headers });

                return response;
            } catch (e) {
                return e;
            }
        },
        async deleteFile({}, {uuid,}) {


            if (!uuid) return;

            try {
                const response = await api.delete(`files/${uuid}`);

                return response;
            } catch (e) {
                return e;
            }
        },

        setTask({commit}, payload = {}) {
            commit('setTask', payload)
        }
    },

    mutations: {
        updateScales(state, scales) {
            state.scales = scales;
        },
        updateMark(state, newMark) {
            newMark.scales.forEach(scale => {
                let foundScale = state.scales.find(item => scale.name === item.name);
                if (!foundScale) return;

                foundScale.value = scale.value;

            });
            state.mark = newMark;

        },
        updateSessionUuid(state, sessionId) {
            state.sessionUuid = sessionId;
        },
        updateExpRespTask(state, payload) {
            state.expRespTask = payload;
            state.expRespTaskLoaded = true;
        },
        updateStore(state, { entity, payload }) {
            state[entity] = payload;
        },
        setTask(state, payload) {
            if (!payload.files) payload.files = []
            if (!payload.files_expert) payload.files_expert = []
            state.task = {
                ...state.task,
                ...payload
            }
        },

        resetNewTask(state) {
            state.task = {
                parameters: {
                    param_type: ''
                },
                files: [],
                files_expert: [],
                mainTest: {
                    TestTitle: ''
                }
            }

            state.filesToAdd.length = 0
            state.filesToDelete.length = 0
        }
    },
}