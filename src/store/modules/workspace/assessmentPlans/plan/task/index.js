import list from './list';
import item from './item';

export default {
  namespaced: true,

	state : {
    ...list.state,
    ...item.state,
  },

	getters : {
    ...list.getters,
    ...item.getters,
  },

	actions : {
    ...list.actions,
    ...item.actions,
  },

	mutations : {
    ...list.mutations,
    ...item.mutations,
  },
}