//import api from "@/network";
import user from './user';

export default {
	state : {
    ...user.state,
  },

	getters : {
    ...user.getters,
  },

	actions : {
    ...user.actions,
  },

	mutations : {
    ...user.mutations,
  },
}