import api from "@/network";

export default {
	state	: {
    userSettings : {
      formSettings : [], // TODO REMOVE
      secutity : [], // TODO REMOVE
    },
    userInfo: null
	},

	getters	: {
    photoUrl ( state )
    {
      if ( state.userSettings !== null )
      {
        return state.userSettings.photo.preview;
      }

      return false;
    },

    userSettings ( state )
    {
      return state.userSettings;
    },


	},

  actions: {





    // TODO REFACTORING
		async fetchUserSettings ( { commit, } ) {


      try
      {
        const response = await api.get( 'user/signup' );

        commit( 'updateUserSettings', response?.data?.data );



        return response;
      }
      catch ( e )
      {


        return Object.assign( {}, e ).response;
      }
    },

    async setUserFormSettingsStore ( { commit, state }, payload )
    {


      let formSettings = state.userSettings.formSettings.map(
        ( formSetting ) => {
          if ( formSetting.uuid === payload.uuid )
          {
            return {
              ...formSetting,

              value : payload.value,
            }
          }
          else
          {
            return formSetting;
          }
        }
      );

      commit(
        'updateUserSettings',

        {
          ...state.userSettings,

          formSettings
        }
      );
    },

    async setUserSettingsStore ( { commit, state }, payload )
    {


      commit(
        'updateUserSettings',

        {
          ...state.userSettings,

          ...payload,
        }
      );
    },

    async uploadUserSettings ({ state }, payload = null) {
      try {
        // fix
        if (payload === null) return await api.put( 'user/settings', state.userSettings );
        else return await api.put( 'user/settings', payload );
      } catch (e){
        return Object.assign( {}, e ).response;
      }
    },
	},

	mutations : {


    // TODO REFACTORING
    updateUserSettings ( state, userSettings )
    {


      state.userSettings = userSettings;
    },
  },
}