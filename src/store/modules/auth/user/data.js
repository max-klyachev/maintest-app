import api from "@/network";

export default {
  state: {
    auth: false,
    user: {
      email: null,
      username: null,
      type: null,
      token: null,
      uuid: null,
      password: null,
    },
  },

  getters: {
    USER_DATA(state) {
      return state.user;
    },

    HAS_AUTH(state) {
      return state.auth;
    },
  },

  actions: {

    async forgotPassword(context, email) {

      try {
        const response = await api.put('/auth/forgot', { email });
        return responses
      } catch (e) {
        return Object.assign({}, e).response;
      }
    },
    async signin({ commit, }, payload) {
      const response = await api.post('auth/login', payload);

      commit('UPDATE_USER_DATA', {
        uuid: response.data.uuid,
        token: response.data.token,
      });



      return response;
    },

    async signup({ commit, }, payload) {
      return await api.post('auth/signup', payload)
          .then((response) => {
            commit('UPDATE_USER_DATA', {
              uuid: response.data.data.uuid,
              token: response.data.data.token,
              status: response.data.data.status,
            });

            return response;
          })

    },

    async confirmPassword({ commit, }, payload) {


      try {
        const response = await api.put('auth/confirm', { password: payload });

        commit('UPDATE_USER_DATA', {
          uuid: response.data.data.uuid,
          token: response.data.data.token,
          status: response.data.data.status,
        });



        return response;
      }
      catch (e) {
        return Object.assign({}, e).response;
      }
    },

    async setUserAuthDataStore({ commit, state }, payload) {


      commit('updateUserDataStore', { ...state.user, ...payload });
    },

    async uploadUserPassword({ state }) {


      try {
        return await api.put('auth/password', state.user);
      }
      catch (e) {


        return Object.assign({}, e).response;
      }
    },
  },

  mutations: {
    UPDATE_USER_DATA(state, payload) {


      // if (!!payload.type) Cookies.set('usertype', payload.type)
      // if (!!payload.email) Cookies.set('useremail', payload.email)
      // if (!!payload.username) Cookies.set('username', payload.username)
      // if (!!payload.token) {
      //   Cookies.set('token', payload.token)
      //   state.auth = true
      // }
      // if (!!payload.uuid) Cookies.set('uuid', payload.uuid)

      let defaultObj = {
        email: null,
        username: null,
        type: null,
        token: null,
        uuid: null,
        password: null,
      }

      let userObj = {
        ...defaultObj,
        ...payload
      }

      state.user = userObj;

      localStorage.setItem('maintest_auth_data', JSON.stringify(payload));
    },

    updateUserDataStore(state, payload) {


      let defaultObj = {
        email: null,
        username: null,
        type: null,
        token: null,
        uuid: null,
        password: null,
      }

      let userObj = {
        ...defaultObj,
        ...payload
      }

      state.user = userObj;
    },
  },
}