//import api from "@/network";
import user from './data';
import settings from './settings';

export default {
	state : {
    ...user.state,
    ...settings.state,
  },

	getters : {
    ...user.getters,
    ...settings.getters,
  },

	actions : {
    ...user.actions,
    ...settings.actions,
  },

	mutations : {
    ...user.mutations,
    ...settings.mutations,
  },
}