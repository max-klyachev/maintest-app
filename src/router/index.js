import { createRouter, createWebHistory } from 'vue-router'

//import guest  from '@/middleware/guest';
import auth from '@/middleware/auth';
import AuthIndex from '@/views/Auth/index';
import SignUp from '@/views/Auth/SignUp';
import SignIn from '@/views/Auth/SignIn';
import ForgotPassword from '@/views/Auth/ForgotPassword';
import SelfRegistration from "@/views/Auth/SelfRegistration.vue";
import Cabinet from "@/views/Auth/Cabinet.vue";

const routes = [

    /* AUTH ROUTS */
    {
        path: '/auth',
        name: 'Auth',
        component: AuthIndex,
        meta: {
            layout: "AuthMainLayout"
        },
        children: [
            {
                path: 'signup',
                name: 'Signup',
                component: SignUp,
            },

            {
                path: 'signin',
                name: 'Signin',
                component: SignIn,
            },
            {
                path: 'forgot',
                name: 'Forgot',
                component: ForgotPassword,
            }
        ],
    },

    {
        path: '/signup',
        name: 'SelfRegistration',
        component: SelfRegistration,
        meta: {
            layout: 'AuthMainLayout'
        }
    },

    /* WORKSPACE ROUTS */

    /* HOME */
    {
        path: '/',
        name: 'Index',
        component: () => import('@/views/workspace/assessmentPlans/pages/plans'),
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
            breadcrumbs: [
                {
                    value: 'Планы оценок', link: 'AssessmentPlansList'
                },
            ],
        },
    },

    /* SUPPORT */
    {
        path: '/support',
        name: 'Index',
        component: () => import('@/views/workspace/assessmentPlans/pages/support'),
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
            breadcrumbs: [
                {
                    value: 'Планы оценок', link: 'AssessmentPlansList'
                },
                {
                    value: 'Поддержка', link: 'support'
                },
            ],
        },
    },

    /* ASSESSMENT PLANS */
    {
        path: '/assessment-plans',
        name: 'AssessmentPlans',
        component: () => import('@/views/workspace/assessmentPlans'),
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
        },
        children: [
            {
                path: '/',
                name: 'AssessmentPlansList',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList' },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans'),
            },

            {
                path: 'archive',
                name: 'AssessmentPlansArchive',
                meta: {
                    breadcrumbs: [
                        {
                            value: 'Планы оценок', link: 'AssessmentPlansList',
                        },
                        {
                            value: 'Архив', link: 'AssessmentPlansArchive',
                        },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/archive-index'),
            },
            {
                path: 'archive/:planUuid',
                name: 'AssessmentPlansArchiveSingle',
                meta: {
                    breadcrumbs: [
                        {
                            value: 'Планы оценок',
                            link: 'AssessmentPlansList',
                        },
                        {
                            value: 'Архив',
                            link: 'AssessmentPlansArchive',
                        },
                        {
                            value: '', link: '', isDynamic: true
                        },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/expRespPlan'),

            },

            {
                path: ':planUuid',
                name: 'ExpRespAssessmentPlanSingle',
                meta: {
                    breadcrumbs: [
                        {
                            value: 'Планы оценок', link: 'AssessmentPlansList',
                        },
                        {
                            value: '', link: '', isDynamic: true
                        },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/expRespPlan'),
                children: [
                    {
                        path: 'tasks/:taskUuid',
                        name: 'RespondentTaskSingle',
                        component: () => import('@/views/workspace/assessmentPlans/pages/plans/expRespPlan/task'),
                    },
                    {
                        path: 'tasks/:taskUuid/respondents/:respondentUuid',
                        name: 'ExpertTaskSingle',
                        component: () => import('@/views/workspace/assessmentPlans/pages/plans/expRespPlan/task'),
                    },
                ],
            },

            // {
            //     path: ':uuid/step1',
            //     name: 'AddEdit1',
            //     component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/step1/step1'),
            // },

            {
                path: ':planUuid/results',
                name: 'AssessmentPlanSingleResults',
                meta: {
                    layout: "WorkspaceMainLayout",
                    middleware: [auth],
                    breadcrumbs: [
                        {
                            value: 'Планы оценок', link: 'AssessmentPlansList'
                        },
                        {
                            value: '',
                            link: 'AddEdit1',
                            isDynamic: true
                        },
                        {
                            value: 'Таблица результатов',
                            link: ''
                        },
                    ],
                },
                component: () => import('@/views/workspace/ResultsTable.vue'),
                children: [
                    {
                        path: 'tasks/:taskUuid/respondents/:respondentUuid',
                        name: 'PlanSingleResultsExpertTaskSingle',
                        component: () => import('@/views/workspace/assessmentPlans/pages/plans/expRespPlan/task'),
                    },
                ]
            },

            {
                path: ':planUuid/respondents/:respondentUuid',
                name: 'RespondentCabinet',
                component: () => import('../views/workspace/Cabinet'),
                meta: {
                    layout: "WorkspaceMainLayout",
                    middleware: [auth],
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList' },
                        { value: '', link: 'AddEdit1' },
                        { value: 'Таблица результатов', link: 'AssessmentPlanSingleResults' },
                        { value: '', link: 'RespondentCabinet' },
                    ]
                },
                children: [
                    {
                        path: 'tasks/:taskUuid',
                        name: 'RespondentCabinetTaskSingle',
                        component: () => import('@/views/workspace/assessmentPlans/pages/plans/expRespPlan/task'),
                    },
                ]
            },

            {
                path: ':uuid/step1',
                name: 'AddEdit1',
                meta: {
                    breadcrumbs: [
                        {
                          value: 'Планы оценок',
                          link: 'AssessmentPlansList'
                        },
                        {
                          AddEdit1: 'AddEdit1',
                          value: 'Редактор плана оценки:',
                          link: ''
                        },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/step1/step1'),
            },

            {
                path: ':uuid_plan/step1/tasks/:uuid_task',
                name: 'AddEdit1Attach',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { AddEdit1: 'AddEdit1', value: 'Редактор плана оценок:', link: '', },
                        { AddEdit1Attach: 'AddEdit1Attach', value: '', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/step1/task'),
            },

            {
                path: ':uuid/step2',
                name: 'AddEdit2',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { AddEdit2: 'AddEdit2', value: 'Редактор плана оценок:', link: '', }
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/step2/step2.vue'),
            },

            {
                path: ':uuid/step3',
                name: 'AddEdit3',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { AddEdit3: 'AddEdit3', value: 'Редактор плана оценок:', link: '', }
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/step3'),
            },

            {
                path: ':uuid/step4',
                name: 'AddEdit4',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { AddEdit4: 'AddEdit4', value: 'Редактор плана оценок:', link: '', }
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/step4'),
            },

            {
                path: ':uuid/settings',
                name: 'PlanSettings',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { PlanSettings: 'PlanSettings', value: 'Настройки плана оценки:', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/settings/Settings'),
            },
            {
                path: ':uuid/settings/notifications',
                name: 'PlanSettingsNotifications',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { PlanSettings: 'PlanSettings', value: '', link: '', },
                        { value: 'Настройки', link: 'PlanSettings', },
                        { value: 'Уведомление пользователей', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/plans/plan/settings/NotificationsSettings'),
            },

            {
                path: ':uuid/exp-resp-correspondence',
                name: 'ExpRespCorrespondence',
                meta: {
                    breadcrumbs: [
                        { value: 'Планы оценок', link: 'AssessmentPlansList', },
                        { ExpRespCorrespondence: 'ExpRespCorrespondence', value: '', link: '', },
                        { value: 'Редактирование', link: '', },
                        { value: 'Шаг 4', link: 'AddEdit4', },
                        { value: 'Соответствие экспертов и респондентов', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/assessmentPlans/pages/expRespCorrespondence'),
            },

            {
                path: 'results',
                name: 'Results',
                component: () => import('@/views/workspace/assessmentPlans/pages/results'),
            },
        ],
    },

    /* TEST LIBRARY */
    {
        path: '/test-library',
        name: 'TestLibrary',
        component: () => import('@/views/workspace/testLibrary'),
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
        },
        children: [
            {
                path: '',
                name: 'TestLibraryIndex',
                meta: {
                    breadcrumbs: [
                        { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                        { value: 'Тесты', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/testLibrary/pages/tests/list'),
            },
            {
                path: 'results',
                name: 'TestLibraryResults',
                meta: {
                    breadcrumbs: [
                        { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                        { value: 'Результаты', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/testLibrary/pages/results'),
            },
            {
                path: 'sessions',
                name: 'TestLibrarySessions',
                meta: {
                    breadcrumbs: [
                        { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                        { value: 'Сеансы', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/testLibrary/pages/sessions'),
            },
            {
                path: 'tests',
                name: 'TestLibraryTests',
                meta: {
                    breadcrumbs: [
                        { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                        { value: 'Тесты', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/testLibrary/pages/tests'),
                children: [
                    {
                        path: '',
                        name: 'TestLibraryTestsIndex',
                        component: () => import('@/views/workspace/testLibrary/pages/tests/list'),
                    },
                    {
                        path: 'list',
                        name: 'TestLibraryTestsList',
                        component: () => import('@/views/workspace/testLibrary/pages/tests/list'),
                    },
                    {
                        path: ':uuid',
                        name: 'TestLibraryTestsItem',
                        component: () => import('@/views/workspace/testLibrary/pages/tests/item'),
                    },
                    {
                        path: ':uuid/settings',
                        name: 'TestLibraryTestsItemSettings',
                        meta: {
                            breadcrumbs: [
                                { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                                { value: 'Тесты', link: 'TestLibraryTestsIndex', },
                                { TestLibraryTestsItemSettings: 'TestLibraryTestsItemSettings', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/testLibrary/pages/tests/item/settings'),
                    },
                    {
                        path: ':uuid/settings/reports/:uuid_variant/block/create',
                        name: 'TestSettingsCreateBlock',
                        meta: {
                            breadcrumbs: [
                                { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                                { value: 'Тесты', link: 'TestLibraryTestsIndex', },
                                {
                                    TestLibraryTestsItemSettings: 'TestLibraryTestsItemSettings',
                                    value: '',
                                    link: 'TestLibraryTestsItemSettings',
                                },
                                { TestSettingsCreateBlock: 'TestSettingsCreateBlock', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/testLibrary/pages/tests/item/settings/block'),
                    },
                    {
                        path: ':uuid/settings/reports/:uuid_variant/block/:uuid_block',
                        name: 'TestSettingsEditBlock',
                        meta: {
                            breadcrumbs: [
                                { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                                { value: 'Тесты', link: 'TestLibraryTestsIndex', },
                                {
                                    TestLibraryTestsItemSettings: 'TestLibraryTestsItemSettings',
                                    value: '',
                                    link: 'TestLibraryTestsItemSettings',
                                },
                                { TestSettingsEditBlock: 'TestSettingsEditBlock', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/testLibrary/pages/tests/item/settings/block'),
                    },
                ],
            },
            {
                path: 'trash',
                name: 'TestLibraryTrash',
                meta: {
                    breadcrumbs: [
                        { value: 'Библиотека тестов', link: 'TestLibraryTestsIndex', },
                        { value: 'Корзина', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/testLibrary/pages/trash'),
            },
        ],
    },

    /* MODEL DEVELOPMENT */
    {
        path: '/model-development',
        name: 'ModelDevelopment',
        component: () => import('../views/workspace/modelDevelopment/ModelDevelopment'),
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
        },
    },

    /* TEST DEVELOPMENT */
    {
        path: '/test-development',
        name: 'TestDevelopment',
        component: () => import('@/views/workspace/testDevelopment'),
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
        },
    },

    /* ADMINISTRATION */
    {
        path: '/administration',
        name: 'Administration',
        component: () => import('../views/workspace/administration'),

        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
        },

        children: [
            {
                path: 'admins',
                name: 'Admins',
                meta: {
                    breadcrumbs: [
                        { value: 'Администрирование', link: 'AdminsList', },
                        { value: 'Администраторы', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/administration/pages/admins'),
                children: [
                    {
                        path: '',
                        name: 'AdminsList',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Администраторы', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/admins/list'),
                    },

                    {
                        path: 'create',
                        name: 'createAdmin',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Администраторы', link: 'AdminsList', },
                                { createAdmin: 'createAdmin', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/admins/id'),
                    },

                    {
                        path: ':id',
                        name: 'Admin',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Администраторы', link: 'AdminsList', },
                                { Admin: 'Admin', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/admins/id'),
                    },
                ],
            },

            {
                path: 'billing',
                name: 'Billing',
                meta: {
                    breadcrumbs: [
                        { value: 'Администрирование', link: 'AdminsList', },
                        { value: 'Биллинг', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/administration/pages/billing'),
            },

            {
                path: 'events',
                name: 'Events',
                meta: {
                    breadcrumbs: [
                        { value: 'Администрирование', link: 'AdminsList', },
                        { value: 'События', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/administration/pages/events'),
            },

            {
                path: 'branchers',
                name: 'Branchers',
                meta: {
                    breadcrumbs: [
                        { value: 'Администрирование', link: 'AdminsList', },
                        { value: 'Филиалы', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/administration/pages/branches'),
                children: [
                    {
                        path: '',
                        name: 'BranchesList',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Филиалы', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/branches/list'),
                    },

                    {
                        path: 'create',
                        name: 'createBranch',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Филиалы', link: 'BranchesList', },
                                { createBranch: 'createBranch', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/branches/id'),
                    },

                    {
                        path: ':id',
                        name: 'Branch',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Филиалы', link: 'BranchesList', },
                                { Branch: 'Branch', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/branches/id'),
                    },
                ],
            },

            {
                path: 'respondents',
                name: 'Respondents',
                meta: {
                    breadcrumbs: [
                        { value: 'Администрирование', link: 'AdminsList', },
                        { value: 'Респонденты', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/administration/pages/respondents'),
                children: [
                    {
                        path: '',
                        name: 'RespondentsList',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Респонденты', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/respondents/list'),
                    },

                    {
                        path: 'create',
                        name: 'createRespondent',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Респонденты', link: 'RespondentsList', },
                                { createRespondent: 'createRespondent', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/respondents/id'),
                    },

                    {
                        path: ':id',
                        name: 'Respondent',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Респонденты', link: 'RespondentsList', },
                                { Respondent: 'Respondent', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/respondents/id'),
                    },
                ],
            },

            {
                path: 'settings',
                name: 'SettingsIndex',
                meta: {
                    breadcrumbs: [
                        { value: 'Администрирование', link: 'AdminsList', },
                        { value: 'Настройка', link: '', },
                    ],
                },
                component: () => import('@/views/workspace/administration/pages/settings'),
                children: [
                    {
                        path: '',
                        name: 'Settings',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Настройка', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/settings/settings'),
                    },

                    {
                        path: 'field',
                        name: 'createField',
                        meta: {
                            breadcrumbs: [
                                { value: 'Администрирование', link: 'AdminsList', },
                                { value: 'Настройка', link: 'Settings', },
                                { value: 'Анкета респондента', link: 'Settings', },
                                { createField: 'createField', value: '', link: '', },
                            ],
                        },
                        component: () => import('@/views/workspace/administration/pages/settings/field'),
                        children: [
                            {
                                path: ':id',
                                name: 'Field',
                                meta: {
                                    breadcrumbs: [
                                        { value: 'Администрирование', link: 'AdminsList', },
                                        { value: 'Настройка', link: 'Settings', },
                                        { value: 'Анкета респондента', link: 'Settings', },
                                        { Field: 'Field', value: '', link: '', },
                                    ],
                                },
                                component: () => import('@/views/workspace/administration/pages/settings/field'),
                            },
                        ],
                    },
                ],
            }
        ],
    },
    {
        path: '/cabinet',
        name: 'UserCabinet',
        meta: {
            layout: "WorkspaceMainLayout",
            middleware: [
                auth,
            ],
            breadcrumbs: [
                { value: 'Главная страница', link: 'AssessmentPlansList', },
                { value: 'Личный кабинет', link: '', },
            ],
        },
        component: Cabinet
    }
];

const router = createRouter(
    {
        history: createWebHistory(process.env.BASE_URL),
        routes,
    }
);

router.beforeEach(
    async (to, from) => { //FIXME //DELETE it is a very bad case
        const isAuth = !!localStorage.getItem('maintest_auth_data');
        const IS_REGISTERED = !!localStorage.getItem('maintest_auth_data') && JSON.parse(localStorage.getItem('maintest_auth_data'))?.status !== 'wait';

        let openRoutes = [
            'Signup',
            'Signin',
            'Forgot',
        ];
        let has = !!(openRoutes.findIndex(route => route === to.name) + 1);


        if (!IS_REGISTERED && to.name !== 'Signin') {
            if (has) {
                return true;
            }
            if (to.name === 'SelfRegistration' && to.query.self_registration) {
                return true;
            }
            else {
                return { name: 'Signin' };
            }
        }
        else if (IS_REGISTERED) {
            if(isAuth && to.name === 'SelfRegistration') {
                return { name: 'Index' };
            }
            if (isAuth && to.name !== 'Index') {
                if (has) {
                    return { name: 'Index' };
                } else {
                    return true;
                }
            }
            if (!isAuth && to.name !== 'Signin') {
                if (has) {
                    return true;
                } else {
                    return { name: 'Signin' };
                }
            }
        }

        // if ( !to.meta.middleware )
        // {
        //   return next();
        // }

        // const middleware = to.meta.middleware;

        // const context = {
        //   to,
        //   from,
        //   next,
        // };


        // return middlewarePipeline( context, middleware, 0 )() ;
    }
);

export default router