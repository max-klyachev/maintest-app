function middlewarePipeline(context, middleware, index) {
  const nextMiddleware = middleware[index];

  if (!nextMiddleware) {
    return context.next;
  }

  return () => {
    nextMiddleware({ ...context, nextMiddleware: middlewarePipeline(context, middleware, ++index) });


  }
}

export default middlewarePipeline;