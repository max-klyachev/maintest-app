import axios from 'axios';
import router from "@/router";

axios.defaults.baseURL = process.env.VUE_APP_API_URL || 'https://maintest.infoshell.ru/api/v1/';
axios.defaults.headers['Content-Type'] = 'application/json';

axios.interceptors.request.use(
  function (config) {
    const token = JSON.parse(localStorage.getItem('maintest_auth_data'))?.token;

    if (token) {
      config.headers.Authorization = `Bearer ${ token }`;
    }

    config.maxContentLength = 500000000;
    config.maxBodyLength = 5000000000;

    return config;
  },
);
axios.interceptors.response.use(
  function (response) {
    if(response.status === 302) {
      location.replace('http://172.27.48.45:7070/');
      return;
    }

    return response;
  },
  function (error) {
      const errorText = error?.response?.data?.errorType;

      if(errorText && errorText === 'emptyRequiredFields') {
          router.push({
              name: 'UserCabinet'
          })
      }

    return Promise.reject(error.response)
  }
)

export default axios;