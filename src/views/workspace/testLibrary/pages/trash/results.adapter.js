export function inputResultsAdapter (results) {


  return results.map((result) => ({
    uuid: result.uuid,
    accomplished: result.acomplished,
    unprocessed: result.unprocessed,
    branch: result.branch,
    deleted: result.deleted,
    mainTest: result.mainTest,
    plans: result.plans,
    reports: result.reports,
    selected: result.selected,
    session: result.session,
    tags: result.tags,
    user: result.user,
  }));
}