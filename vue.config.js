module.exports = {
  css: {
    extract: false,
    loaderOptions: {
      scss: {
        prependData: `
          @import "@/assets/scss/partials/_vars.scss";
          @import "@/assets/scss/partials/_media.scss";
        `
      }
    }
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.pug$/,
          oneOf: [
            {
              resourceQuery: /^\?vue/,
              use: ['pug-plain-loader']
            },
            {
              use: ['raw-loader', 'pug-plain-loader']
            }
          ]
        },
      ]
    }
  }
}