# maintest-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


Code Style Rules:

FOLDERS:
- components
  1. workspace. Components with business logic
     - create and arrange by folder components based on page. 
  2. UI
       - use **mt** prefix for new UI components
  3. composite. UI Components based on multiple component

Basic snippet for components:

````
<template lang="pug">

</template>
<script>
  export default {
    components: {},
    props: {},
    data: () => ({}),
    computed: {},
    watch: {},
    methods: {
      /* GETTERS */

      /* SETTERS */

      /* HANDLERS */

      /* HELPERS */

      /* ACTIONS */
    },
    created () {},
    mounted () {}
  }
</script>
<style lang="scss"></style>
````

Some notes about naming:
- "CustomAssessment" - Модальное окно для экспертной оценки. Наследуется для следующих компонентов:
    "RespondentCustomAssessment" включает через слоты блок для оценки задания
    "ExpertCustomAssessment" "наследуется" от компонента выше, включает через слоты блок для оценки задания